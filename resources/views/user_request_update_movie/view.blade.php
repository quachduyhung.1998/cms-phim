@extends('layouts.app')
@section('title', __('Yêu cầu cập nhật phim'))

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10 d-lg-none">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
                <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                    <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                        <h1 class="page-heading d-flex flex-column justify-content-center text-gray-900 fw-bold fs-3 m-0">@yield('title')</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('dashboard') }}" class="text-muted text-hover-primary">{{ __('Tổng quan') }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-500 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">@yield('title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <div id="kt_app_content_container" class="app-container container-fluid">
                <div class="card">
                    <div class="card-body">
                        <h1 class="mb-5">{{ __('Thông tin tổng quan') }}</h1>
                        <div class="table-responsive mb-5">
                            <table class="table table-row-dashed">
                                <tr>
                                    <td class="align-middle fw-semibold w-150px">{{ __('Biên tập viên') }}</td>
                                    <td>
                                        <div class="fw-bold mb-1">{{ $userRequestUpdateMovie->user?->full_name }}</div>
                                        <span>{{ $userRequestUpdateMovie->user?->email }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fw-semibold w-150px">{{ __('Phim') }}</td>
                                    <td><a href="{{ route('movie.view', $userRequestUpdateMovie->movie_id) }}" target="_blank" class="fw-bold text-gray-800 text-hover-primary">{{ $userRequestUpdateMovie->movie?->name_vn }}</a></td>
                                </tr>
                                <tr>
                                    <td class="fw-semibold w-150px">{{ __('Yêu cầu lúc') }}</td>
                                    <td>{{ $userRequestUpdateMovie->created_at->format('H:i - d/m/Y') }}</td>
                                </tr>
                                <tr>
                                    <td class="fw-semibold w-150px">{{ __('Trạng thái') }}</td>
                                    <td><span class="badge badge-light-{{ \App\Models\UserRequestUpdateMovie::$statusColor[$userRequestUpdateMovie->status] }}">{{ \App\Models\UserRequestUpdateMovie::$statusLabel[$userRequestUpdateMovie->status] }}</span></td>
                                </tr>
                                <tr>
                                    <td class="fw-semibold w-150px">{{ __('Admin cập nhật') }}</td>
                                    <td>{{ $userRequestUpdateMovie->admin?->full_name }}</td>
                                </tr>
                            </table>
                        </div>
                        @if($userRequestUpdateMovie->status === \App\Models\UserRequestUpdateMovie::STATUS_INIT)
                            <a href="#" class="btn btn-primary btn-sm btn-update-status" data-status="{{ \App\Models\UserRequestUpdateMovie::STATUS_APPROVE }}">{{ __('Phê duyệt') }}</a>
                            <a href="#" class="btn btn-danger btn-sm btn-update-status" data-status="{{ \App\Models\UserRequestUpdateMovie::STATUS_REJECT }}">{{ __('Từ chối') }}</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@push('js')
    <script>
        $('.btn-update-status').click(function() {
            const status = $(this).data('status')

            let icon = 'success'
            let color = 'success'
            let title = 'Phê duyệt'
            let html = 'Yêu cầu này sẽ được phê duyệt, biên tập viên yêu cầu sẽ có quyền cập nhật phim'
            if (status == '{{ \App\Models\UserRequestUpdateMovie::STATUS_REJECT }}') {
                icon = 'error'
                color = 'danger'
                title = 'Từ chối'
                html = 'Yêu cầu này sẽ bị từ chối, biên tập viên yêu cầu sẽ không có quyền cập nhật phim'
            }
            Swal.fire({
                title,
                html,
                icon,
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "{{ __('Xác nhận') }}",
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: `btn btn-${color}`,
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ route('user_request_update_movies.update') }}',
                        type: 'POST',
                        data: {
                            _token: '{{ csrf_token() }}',
                            id: '{{ $userRequestUpdateMovie->id }}',
                            status
                        },
                        beforeSend: KTApp.showPageLoading(),
                        success: function (response) {
                            if (response.status) {
                                toastr.success("{{ __('Cập nhật thành công') }}");
                                setTimeout(function() {
                                    window.location.reload()
                                }, 1500)
                            } else {
                                toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                            }
                        }
                    }).always(function () {
                        KTApp.hidePageLoading()
                    })
                }
            })
        })
    </script>
@endpush
