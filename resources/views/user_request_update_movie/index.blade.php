@extends('layouts.app')
@section('title', __('Danh sách yêu cầu cập nhật phim'))

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10 d-lg-none">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
                <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                    <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                        <h1 class="page-heading d-flex flex-column justify-content-center text-gray-900 fw-bold fs-3 m-0">@yield('title')</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('dashboard') }}" class="text-muted text-hover-primary">{{ __('Tổng quan') }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-500 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">@yield('title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <div id="kt_app_content_container" class="app-container container-fluid">
                <div class="card">
                    <!--begin::Card header-->
                    <div class="card-header border-0 pt-6">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <!--begin::Search-->
                            <form action="{{ route('user_request_update_movies.index') }}" class="d-lg-flex align-items-center gap-2">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <i class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></i>
                                    <input type="text" name="keyword" value="{{ request('keyword') }}" data-kt-user_request_update_movie-table-filter="search" class="form-control w-lg-250px w-100 ps-13" placeholder="{{ __('Từ khóa') }}" />
                                </div>
                                <button type="submit" class="btn btn-primary w-100 fw-semibold px-6">{{ __('Tìm kiếm') }}</button>
                            </form>
                            <!--end::Search-->
                        </div>
                    </div>
                    <div class="card-body table-responsive py-4">
                        <table class="table align-middle table-row-dashed table-responsive fs-6 gy-5" id="kt_table_user_request_update_movies">
                            <thead>
                                <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                    <th class="text-center">{{ __('STT') }}</th>
                                    <th class="min-w-125px">{{ __('Biên tập viên') }}</th>
                                    <th class="min-w-125px">{{ __('Phim') }}</th>
                                    <th class="min-w-125px">{{ __('Yêu cầu lúc') }}</th>
                                    <th class="min-w-125px text-center">{{ __('Trạng thái') }}</th>
                                    <th class="min-w-125px">{{ __('Admin cập nhật') }}</th>
                                    <th class="text-end min-w-100px">{{ __('Thao tác') }}</th>
                                </tr>
                            </thead>
                            <tbody class="text-gray-600 fw-semibold">
                                @if ($userRequestUpdateMovies->total() == 0)
                                    <tr><td colspan="8">{{ __('Chưa có dữ liệu') }}</td></tr>
                                @else
                                    @foreach($userRequestUpdateMovies as $index => $userRequestUpdateMovie)
                                        <tr>
                                            <td class="text-center">{{ ($userRequestUpdateMovies->currentPage() - 1) * $userRequestUpdateMovies->perPage() + $index+1 }}</td>
                                            <td>
                                                <div class="d-flex flex-column">
                                                    <div class="text-gray-800 mb-1">{{ $userRequestUpdateMovie->user?->full_name }}</div>
                                                    <span>{{ $userRequestUpdateMovie->user?->email }}</span>
                                                </div>
                                            </td>
                                            <td><a href="{{ route('movie.view', $userRequestUpdateMovie->movie_id) }}" target="_blank" class="text-gray-800 text-hover-primary">{{ $userRequestUpdateMovie->movie?->name_vn }}</a></td>
                                            <td>{{ $userRequestUpdateMovie->created_at->format('H:i - d/m/Y') }}</td>
                                            <td class="text-center">
                                                <span class="badge badge-light-{{ \App\Models\UserRequestUpdateMovie::$statusColor[$userRequestUpdateMovie->status] }}">{{ \App\Models\UserRequestUpdateMovie::$statusLabel[$userRequestUpdateMovie->status] }}</span>
                                            </td>
                                            <td>{{ $userRequestUpdateMovie->admin?->full_name }}</td>
                                            <td class="text-end">
                                                @if($userRequestUpdateMovie->status === \App\Models\UserRequestUpdateMovie::STATUS_INIT)
                                                    <a href="#" class="btn btn-light-primary btn-sm btn-update-status" data-id="{{ $userRequestUpdateMovie->id }}" data-status="{{ \App\Models\UserRequestUpdateMovie::STATUS_APPROVE }}">{{ __('Phê duyệt') }}</a>
                                                    <a href="#" class="btn btn-light-danger btn-sm btn-update-status" data-id="{{ $userRequestUpdateMovie->id }}" data-status="{{ \App\Models\UserRequestUpdateMovie::STATUS_REJECT }}">{{ __('Từ chối') }}</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        {{ $userRequestUpdateMovies->appends($_GET)->links('layouts.paginate') }}
                    </div>
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@push('js')
    <script>
        $('.btn-update-status').click(function() {
            const id = $(this).data('id')
            const status = $(this).data('status')

            let icon = 'success'
            let color = 'success'
            let title = 'Phê duyệt'
            let html = 'Yêu cầu này sẽ được phê duyệt, biên tập viên yêu cầu sẽ có quyền cập nhật phim'
            if (status == '{{ \App\Models\UserRequestUpdateMovie::STATUS_REJECT }}') {
                icon = 'error'
                color = 'danger'
                title = 'Từ chối'
                html = 'Yêu cầu này sẽ bị từ chối, biên tập viên yêu cầu sẽ không có quyền cập nhật phim'
            }
            Swal.fire({
                title,
                html,
                icon,
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "{{ __('Xác nhận') }}",
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: `btn btn-${color}`,
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ route('user_request_update_movies.update') }}',
                        type: 'POST',
                        data: {
                            _token: '{{ csrf_token() }}',
                            id,
                            status
                        },
                        beforeSend: KTApp.showPageLoading(),
                        success: function (response) {
                            if (response.status) {
                                toastr.success("{{ __('Cập nhật thành công') }}");
                                setTimeout(function() {
                                    window.location.reload()
                                }, 1500)
                            } else {
                                toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                            }
                        }
                    }).always(function () {
                        KTApp.hidePageLoading()
                    })
                }
            })
        })
    </script>
@endpush
