@extends('layouts._blank')
@section('title', __('500'))

@push('css')
    <style>
        body {
            background-image: url('{{ asset('assets/media/auth/bg1.jpg') }}');
        }
        [data-bs-theme="dark"] body {
            background-image: url('{{ asset('assets/media/auth/bg1-dark.jpg') }}');
        }
    </style>
@endpush

@section('content')
    <div class="d-flex flex-column flex-center flex-column-fluid">
        <div class="d-flex flex-column flex-center text-center p-10">
            <div class="card card-flush w-lg-650px py-5">
                <div class="card-body py-15 py-lg-20">
                    <h1 class="fw-bolder fs-2qx text-gray-900 mb-4">{{ __('Lỗi hệ thống') }}</h1>
                    <div class="fw-semibold fs-6 text-gray-500 mb-2">{{ __('Đã có lỗi xảy ra! Chúng tôi sẽ khắc phục trong thời gian sớm nhất.') }}</div>
                    <div class="fw-semibold fs-6 text-gray-500 mb-7">{{ __('Bạn vui lòng quay lại sau') }}</div>
                    <div class="mb-11">
                        <img src="{{ asset('assets/media/auth/500-error.png') }}" class="mw-100 mh-300px theme-light-show" alt="" />
                        <img src="{{ asset('assets/media/auth/500-error-dark.png') }}" class="mw-100 mh-300px theme-dark-show" alt="" />
                    </div>
                    <div class="mb-0">
                        <a href="{{ url()->previous() }}" class="btn btn-sm btn-primary">{{ __('Trở về') }}</a>
                    </div>
                </div>
            </div>
        </div>
@endsection
