<div class="modal-content">
    <div class="modal-header" id="kt_modal_update_actor_header">
        <h2 class="fw-bold">{{ __('Cập nhật diễn viên') }}</h2>
        <div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal" aria-label="Close">
            <i class="ki-outline ki-cross fs-1"></i>
        </div>
    </div>
    <div class="modal-body px-5">
        <form id="kt_modal_update_actor_form" class="form">
            @csrf
            <input type="hidden" name="id" value="{{ $actor->id }}">
            <div class="d-flex flex-column scroll-y px-5 px-lg-10" id="kt_modal_update_actor_scroll" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_update_actor_header" data-kt-scroll-wrappers="#kt_modal_update_actor_scroll" data-kt-scroll-offset="300px">
                <div class="fv-row mb-7">
                    <label class="required fw-semibold fs-6 mb-2">{{ __('Tên gói') }}</label>
                    <input type="text" name="full_name" class="form-control mb-3 mb-lg-0" value="{{ $actor->full_name }}" />
                    <span class="text-danger d-block mt-1 box-err-full_name"></span>
                </div>
                <div class="fv-row mb-7">
                    <label class="fw-semibold fs-6 mb-2">{{ __('Ảnh') }}</label>
                    <!--begin::Dropzone-->
                    <div class="dropzone" id="dropzonejs_image_actor_update">
                        <input type="hidden" name="image" class="form-control mb-3 mb-lg-0" value="{{ old('image', isset($actor) ? $actor->image : '') }}" />
                        <div class="d-flex flex-xl-row flex-column-reverse align-center justify-content-between gap-2">
                            <div class="dz-message needsclick align-items-center flex-shrink-0 mt-xl-0 mt-5">
                                <i class="ki-duotone ki-file-up fs-3x text-primary"><span class="path1"></span><span class="path2"></span></i>
                                <div class="ms-4">
                                    <h3 class="fs-5 fw-bold text-gray-900 mb-1">{{ __('Thêm hình ảnh') }}</h3>
                                    <div class="fs-7 fw-semibold text-gray-500">{{ __('Kéo và thả tập tin vào đây hoặc click chọn') }}</div>
                                    <div class="fs-7 fw-semibold text-gray-500">{{ __('Loại ảnh cho phép: png, jpeg, jpg') }}</div>
                                    <div class="fs-7 fw-semibold text-gray-500">{{ __('Dung lượng tối đa: 10MB') }}</div>
                                </div>
                            </div>
                            <a href="{{ $actor->image ? asset($actor->image) : '' }}" class="text-center image_preview" target="_blank" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem ảnh') }}">
                                <img src="{{ $actor->image ? asset($actor->image) : '' }}" alt="" class="cursor-pointer mw-100 mh-200px">
                            </a>
                        </div>
                    </div>
                    <!--end::Dropzone-->
                </div>
                <div class="fv-row mb-7">
                    <label class="fw-semibold fs-6 mb-2">{{ __('Mô tả') }}</label>
                    <textarea name="description" id="kt_docs_ckeditor_classic_update">{{ $actor->description }}</textarea>
                    <span class="text-danger d-block mt-1 box-err-description"></span>
                </div>
            </div>
            <div class="text-center">
                <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" data-kt-categories-modal-action="cancel">{{ __('Hủy') }}</button>
                <button type="button" id="btn-submit-update-actor" class="btn btn-primary" data-kt-categories-modal-action="submit">
                    <span class="indicator-label">{{ __('Cập nhật') }}</span>
                    <span class="indicator-progress">{{ __('Đang cập nhật') }}...
                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                        </span>
                </button>
            </div>
        </form>
    </div>
</div>

<script>
    formatInputNumber()

    initDropzoneUploadOneFile('#dropzonejs_image_actor_update')

    ClassicEditor
        .create(document.querySelector('#kt_docs_ckeditor_classic_update'), {
            toolbar: ['undo', 'redo', '|', 'heading', '|', 'bold', 'italic', '|', 'blockQuote', 'bulletedList', 'numberedList', 'outdent', 'indent']
        })
        .then(editor => {
            description = editor
        })
        .catch(error => {
            console.error(error);
        });
</script>
