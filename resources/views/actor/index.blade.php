@extends('layouts.app')
@section('title', __('Diễn viên'))

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10 d-lg-none">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
                <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                    <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                        <h1 class="page-heading d-flex flex-column justify-content-center text-gray-900 fw-bold fs-3 m-0">@yield('title')</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('dashboard') }}" class="text-muted text-hover-primary">{{ __('Tổng quan') }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-500 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">@yield('title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <div id="kt_app_content_container" class="app-container container-fluid">
                <div class="card">
                    <!--begin::Card header-->
                    <div class="card-header border-0 pt-6">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <!--begin::Search-->
                            <form action="{{ route('actor.index') }}" class="d-lg-flex align-items-center gap-2">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <i class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></i>
                                    <input type="text" name="keyword" value="{{ request('keyword') }}" data-kt-actor-table-filter="search" class="form-control w-lg-250px w-100 ps-13" placeholder="{{ __('Từ khóa') }}" />
                                </div>
                                <button type="submit" class="btn btn-primary w-100 fw-semibold px-6">{{ __('Tìm kiếm') }}</button>
                            </form>
                            <!--end::Search-->
                        </div>
                        <!--begin::Card toolbar-->
                        <div class="card-toolbar">
                            <div class="d-flex justify-content-end">
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_add_actor">
                                    <i class="ki-outline ki-plus fs-2"></i>{{ __('Thêm diễn viên') }}
                                </button>
                            </div>
                        </div>
                        <!--end::Card toolbar-->
                    </div>
                    <div class="card-body table-responsive py-4">
                        <table class="table align-middle table-row-dashed table-responsive fs-6 gy-5" id="kt_table_actors">
                            <thead>
                                <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                    <th class="text-center">{{ __('STT') }}</th>
                                    <th class="min-w-125px">{{ __('Ảnh') }}</th>
                                    <th class="min-w-125px">{{ __('Diễn viên') }}</th>
                                    <th class="min-w-125px">{{ __('Mô tả') }}</th>
                                    <th class="min-w-125px">{{ __('Ngày tạo') }}</th>
                                    <th class="text-end min-w-100px">{{ __('Thao tác') }}</th>
                                </tr>
                            </thead>
                            <tbody class="text-gray-600 fw-semibold">
                                @if ($actors->total() == 0)
                                    <tr><td colspan="8">{{ __('Chưa có dữ liệu') }}</td></tr>
                                @else
                                    @php
                                        $prev_color = $current_color = '';
                                    @endphp
                                    @foreach($actors as $index => $actor)
                                        @php
                                            while ($current_color == $prev_color) {
                                                $current_color = config('default.color')[rand(1, 5)];
                                            }
                                            $prev_color = $current_color;
                                        @endphp
                                        <tr>
                                            <td class="text-center">{{ ($actors->currentPage() - 1) * $actors->perPage() + $index+1 }}</td>
                                            <td>
                                                @if($actor->image)
                                                    <img src="{{ $actor->image }}" alt="" class="w-80px rounded">
                                                @else
                                                    <div class="symbol symbol-circle symbol-50px overflow-hidden me-3">
                                                        <a href="#" class="btn-edit-actor" data-url="{{ route('actor.edit', $actor->id) }}">
                                                            <div class="symbol-label fs-3 bg-light-{{ $current_color }} text-{{ $current_color }}">{{ strtoupper(mb_substr(last(explode(' ', $actor->full_name)), 0, 1, 'UTF-8')) }}</div>
                                                        </a>
                                                    </div>
                                                @endif
                                            </td>
                                            <td>{{ $actor->full_name }}</td>
                                            <td><div class="text-limit-5-line mw-600px">{!! $actor->description !!}</div></td>
                                            <td>{{ $actor->created_at->format('H:i - d/m/Y') }}</td>
                                            <td class="text-end">
                                                <a href="#" class="btn btn-light btn-active-light-primary btn-flex btn-center btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{{ __('Thao tác') }}
                                                    <i class="ki-outline ki-down fs-5 ms-1"></i>
                                                </a>
                                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" data-kt-menu="true">
                                                    <div class="menu-item px-3">
                                                        <a href="#" class="menu-link px-3 btn-edit-actor" data-url="{{ route('actor.edit', $actor->id) }}">{{ __('Cập nhật') }}</a>
                                                    </div>
                                                    <div class="menu-item px-3">
                                                        <a href="#" class="menu-link px-3 text-danger btn-delete-actor" data-id="{{ $actor->id }}" data-name="{{ $actor->name }}">{{ __('Xóa') }}</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        {{ $actors->appends($_GET)->links('layouts.paginate') }}
                    </div>
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>

    <!--begin::Modal - Add actor-->
    <div class="modal fade" id="kt_modal_add_actor" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <div class="modal-content">
                <div class="modal-header" id="kt_modal_add_actor_header">
                    <h2 class="fw-bold">{{ __('Thêm diễn viên') }}</h2>
                    <div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-outline ki-cross fs-1"></i>
                    </div>
                </div>
                <div class="modal-body px-5">
                    <form id="kt_modal_add_actor_form" class="form">
                        @csrf
                        <div class="d-flex flex-column scroll-y px-5 px-lg-10" id="kt_modal_add_actor_scroll" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_actor_header" data-kt-scroll-wrappers="#kt_modal_add_actor_scroll" data-kt-scroll-offset="300px">
                            <div class="fv-row mb-7">
                                <label class="required fw-semibold fs-6 mb-2">{{ __('Tên diễn viên') }}</label>
                                <input type="text" name="full_name" class="form-control mb-3 mb-lg-0" />
                                <span class="text-danger d-block mt-1 box-err-full_name"></span>
                            </div>
                            <div class="fv-row mb-7">
                                <label class="fw-semibold fs-6 mb-2">{{ __('Ảnh') }}</label>
                                <!--begin::Dropzone-->
                                <div class="dropzone" id="dropzonejs_image_actor">
                                    <input type="hidden" name="image" class="form-control mb-3 mb-lg-0" value="" />
                                    <div class="d-flex flex-xl-row flex-column-reverse align-center justify-content-between gap-2">
                                        <div class="dz-message needsclick align-items-center flex-shrink-0 mt-xl-0 mt-5">
                                            <i class="ki-duotone ki-file-up fs-3x text-primary"><span class="path1"></span><span class="path2"></span></i>
                                            <div class="ms-4">
                                                <h3 class="fs-5 fw-bold text-gray-900 mb-1">{{ __('Thêm hình ảnh') }}</h3>
                                                <div class="fs-7 fw-semibold text-gray-500">{{ __('Kéo và thả tập tin vào đây hoặc click chọn') }}</div>
                                                <div class="fs-7 fw-semibold text-gray-500">{{ __('Loại ảnh cho phép: png, jpeg, jpg') }}</div>
                                                <div class="fs-7 fw-semibold text-gray-500">{{ __('Dung lượng tối đa: 10MB') }}</div>
                                            </div>
                                        </div>
                                        <a href="" class="text-center image_preview" target="_blank" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem ảnh') }}">
                                            <img src="" alt="" class="cursor-pointer mw-100 mh-200px">
                                        </a>
                                    </div>
                                </div>
                                <!--end::Dropzone-->
                            </div>
                            <div class="fv-row mb-7">
                                <label class="fw-semibold fs-6 mb-2">{{ __('Mô tả') }}</label>
                                <textarea name="description" id="kt_docs_ckeditor_classic"></textarea>
                                <span class="text-danger d-block mt-1 box-err-description"></span>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" data-kt-actors-modal-action="cancel">{{ __('Hủy') }}</button>
                            <button type="button" id="btn-submit-add-actor" class="btn btn-primary" data-kt-actors-modal-action="submit">
                                <span class="indicator-label">{{ __('Lưu') }}</span>
                                <span class="indicator-progress">{{ __('Đang lưu') }}...
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Modal - Add actor-->

    <!--begin::Modal - Update actor-->
    <div class="modal fade" id="kt_modal_update_actor" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js') }}"></script>
    <script>
        initDropzoneUploadOneFile('#dropzonejs_image_actor')

        let description;
        ClassicEditor
            .create(document.querySelector('#kt_docs_ckeditor_classic'), {
                toolbar: ['undo', 'redo', '|', 'heading', '|', 'bold', 'italic', '|', 'blockQuote', 'bulletedList', 'numberedList', 'outdent', 'indent']
            })
            .then(editor => {
                description = editor
            })
            .catch(error => {
                console.error(error);
            });

        $('#btn-submit-add-actor').click(function() {
            $(this).find('.indicator-label').css('display', 'none')
            $(this).find('.indicator-progress').css('display', 'block')
            $(this).attr('disabled', true)
            let full_name = $('#kt_modal_add_actor_form input[name="full_name"]').val()
            let image = $('#kt_modal_add_actor_form input[name="image"]').val()
            $.ajax({
                url: '{{ route('actor.store') }}',
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    full_name,
                    image,
                    description: description.getData(),
                },
                success: function (result) {
                    if (result.status) {
                        Swal.fire({
                            text: "{{ __('Thêm diễn viên thành công') }}",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        })

                        $('#kt_modal_add_actor').modal('hide')

                        setTimeout(function() {
                            window.location.reload()
                        }, 1500)
                    } else {
                        toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                    }
                },
                error: function(error) {
                    const errors = error.responseJSON.errors
                    if (errors.name) {
                        $('#kt_modal_add_actor_form .box-err-name').html(errors.name[0])
                        $('#kt_modal_add_actor_form input[name="name"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_add_actor_form .box-err-name').html('')
                        $('#kt_modal_add_actor_form input[name="name"]').removeClass('border-danger')
                    }
                }
            }).always(function () {
                $('#btn-submit-add-actor .indicator-label').removeAttr('style')
                $('#btn-submit-add-actor .indicator-progress').removeAttr('style')
                $('#btn-submit-add-actor').attr('disabled', false)
            })
        })

        $('.btn-edit-actor').click(function() {
            const url = $(this).data('url')
            $.ajax({
                url,
                beforeSend: KTApp.showPageLoading(),
                success: function(response) {
                    $('#kt_modal_update_actor .modal-dialog').html(response)
                    $('#kt_modal_update_actor').modal('show')
                }
            }).always(function() {
                KTApp.hidePageLoading()
            })
        })

        $(document).on('click', '#btn-submit-update-actor', function() {
            $(this).find('.indicator-label').css('display', 'none')
            $(this).find('.indicator-progress').css('display', 'block')
            $(this).attr('disabled', true)
            let url = '{{ route('actor.update') }}'
            let id = $('#kt_modal_update_actor_form input[name="id"]').val()
            let full_name = $('#kt_modal_update_actor_form input[name="full_name"]').val()
            let image = $('#kt_modal_update_actor_form input[name="image"]').val()
            $.ajax({
                url,
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    id,
                    full_name,
                    image,
                    description: description.getData(),
                },
                success: function (result) {
                    if (result.status) {
                        Swal.fire({
                            text: "{{ __('Cập nhật diễn viên thành công') }}",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        })

                        $('#kt_modal_update_actor').modal('hide')

                        setTimeout(function() {
                            window.location.reload()
                        }, 1500)
                    } else {
                        toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                    }
                },
                error: function(error) {
                    const errors = error.responseJSON.errors
                    if (errors.name) {
                        $('#kt_modal_update_actor_form .box-err-name').html(errors.name[0])
                        $('#kt_modal_update_actor_form input[name="name"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_update_actor_form .box-err-name').html('')
                        $('#kt_modal_update_actor_form input[name="name"]').removeClass('border-danger')
                    }
                }
            }).always(function () {
                $('#btn-submit-update-actor .indicator-label').removeAttr('style')
                $('#btn-submit-update-actor .indicator-progress').removeAttr('style')
                $('#btn-submit-update-actor').attr('disabled', false)
            })
        })

        $('.btn-delete-actor').click(function() {
            const id = $(this).data('id')
            const name = $(this).data('name')
            Swal.fire({
                title: "{{ __('Xóa diễn viên') }}",
                html: "{{ __('Bạn có chắc muốn xóa diễn viên') }} <b>" + name + '</b>?',
                icon: "warning",
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "{{ __('Xóa') }}",
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ route('actor.delete') }}',
                        type: 'DELETE',
                        data: {
                            _token: '{{ csrf_token() }}',
                            id
                        },
                        beforeSend: KTApp.showPageLoading(),
                        success: function (response) {
                            if (response.status) {
                                toastr.success("{{ __('Xóa diễn viên thành công') }}");
                                setTimeout(function() {
                                    window.location.reload()
                                }, 1500)
                            } else {
                                toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                            }
                        }
                    }).always(function () {
                        KTApp.hidePageLoading()
                    })
                }
            })
        })
    </script>
@endpush
