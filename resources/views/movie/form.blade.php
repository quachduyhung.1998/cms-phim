<div class="row">
    <div class="col-md-6 col-12 mb-7">
        <div class="row">
            <div class="col-12 mb-7">
                <label class="required fw-semibold fs-6 mb-2">{{ __('Tên phim tếng Việt') }}</label>
                <input type="text" name="name_vn" class="form-control mb-3 mb-lg-0 @error('name_vn') border-danger @enderror" value="{{ old('name', isset($movie) ? $movie->name_vn : '') }}" />
                @error('name_vn')
                    <span class="text-danger d-block mt-1">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-12 mb-7">
                <label class="required fw-semibold fs-6 mb-2">{{ __('Tên phim tiếng Anh') }}</label>
                <input type="text" name="name_en" class="form-control mb-3 mb-lg-0 @error('name_en') border-danger @enderror" value="{{ old('name_en', isset($movie) ? $movie->name_en : '') }}" />
                @error('name_en')
                    <span class="text-danger d-block mt-1">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-12" style="margin-bottom: 27px;">
                <label class="fw-semibold fs-6 mb-2">{{ __('Ảnh') }}</label>
                <!--begin::Dropzone-->
                <div class="dropzone" id="dropzonejs_image_movie">
                    <div class="dz-message needsclick align-items-center flex-shrink-0 mt-xl-0 mt-5">
                        <i class="ki-duotone ki-file-up fs-3x text-primary"><span class="path1"></span><span class="path2"></span></i>
                        <div class="ms-4">
                            <h3 class="fs-5 fw-bold text-gray-900 mb-1">{{ __('Thêm hình ảnh') }}</h3>
                            <div class="fs-7 fw-semibold text-gray-500">{{ __('Kéo và thả tập tin vào đây hoặc click chọn') }}</div>
                            <div class="fs-7 fw-semibold text-gray-500">{{ __('Loại ảnh cho phép: png, jpeg, jpg') }}</div>
                            <div class="fs-7 fw-semibold text-gray-500">{{ __('Dung lượng tối đa: 10MB') }}</div>
                        </div>
                    </div>
                </div>
                <!--end::Dropzone-->

                <div class="d-flex flex-wrap align-items-center gap-2 mt-5 list-image">
                    @if(old('images'))
                        @foreach(old('images') as $image)
                            <div class="image-item">
                                <div class="overlay-layer"></div>
                                <div class="d-flex align-items-center gap-1 group-btn-image">
                                    <a href="{{ asset($image) }}" target="_blank" class="lh-1" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem ảnh') }}"><i class="ki-solid ki-eye fs-2 text-success"></i></a>
                                    <i class="ki-duotone ki-trash-square fs-1 text-danger cursor-pointer btn-remove-image" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xóa') }}">
                                        <span class="path1"></span>
                                        <span class="path2"></span>
                                        <span class="path3"></span>
                                        <span class="path4"></span>
                                    </i>
                                </div>
                                <input type="hidden" name="images[]" value="{{ $image }}">
                                <img src="{{ $image }}" alt="{{ $movie->name ?? '' }}" class="w-md-100px w-90px border rounded shadow">
                            </div>
                        @endforeach
                    @elseif(isset($movie) && $movie->images)
                        @foreach($movie->images as $image)
                            <div class="image-item">
                                <div class="overlay-layer"></div>
                                <div class="d-flex align-items-center gap-1 group-btn-image">
                                    <a href="{{ asset($image) }}" target="_blank" class="lh-1" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem ảnh') }}"><i class="ki-solid ki-eye fs-2 text-success"></i></a>
                                    <i class="ki-duotone ki-trash-square fs-1 text-danger cursor-pointer btn-remove-image" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xóa') }}">
                                        <span class="path1"></span>
                                        <span class="path2"></span>
                                        <span class="path3"></span>
                                        <span class="path4"></span>
                                    </i>
                                </div>
                                <input type="hidden" name="images[]" value="{{ $image }}">
                                <img src="{{ $image }}" alt="{{ $movie->name ?? '' }}" class="w-md-100px w-90px border rounded shadow">
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Tên tập phim') }}</label>
                <input type="text" name="practice" class="form-control mb-3 mb-lg-0" value="{{ old('practice', isset($movie) ? $movie->practice : '') }}" />
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Trailer') }}</label>
                <input type="text" name="trailer" class="form-control mb-3 mb-lg-0" value="{{ old('trailer', isset($movie) ? $movie->trailer : '') }}" />
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Thời gian bắt đầu phát intro') }}</label>
                <input type="text" name="intro_start" class="form-control mb-3 mb-lg-0" value="{{ old('intro_start', isset($movie) ? $movie->intro_start : '') }}" placeholder="VD: 01:30:26" />
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Thời gian kết thúc intro') }}</label>
                <input type="text" name="intro_end" class="form-control mb-3 mb-lg-0" value="{{ old('intro_end', isset($movie) ? $movie->intro_end : '') }}" placeholder="VD: 01:30:26" />
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Outro') }}</label>
                <input type="text" name="outro" class="form-control mb-3 mb-lg-0" value="{{ old('outro', isset($movie) ? $movie->outro : '') }}" placeholder="VD: 01:30:26" />
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Thể loại') }}</label>
                <select name="category_id" class="form-select fw-bold" data-kt-select2="true" data-placeholder="{{ __('Chọn thể loại') }}" data-allow-clear="true" data-hide-search="true">
                    <option></option>
                    @foreach($categories as $key => $value)
                        <option value="{{ $key }}" {{ isset($movie) && $movie->category_id === $key ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Quốc gia') }}</label>
                <input type="text" name="country" class="form-control mb-3 mb-lg-0" value="{{ old('country', isset($movie) ? $movie->country : '') }}" />
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Tóm tắt') }}</label>
                <textarea name="vn_summary" id="kt_docs_ckeditor_classic">{{ old('vn_summary', isset($movie) ? $movie->vn_summary : '') }}</textarea>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-12 mb-7">
        <div class="row">
            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Thời lượng') }}</label>
                <input type="text" name="duration" class="form-control mb-3 mb-lg-0" value="{{ old('duration', isset($movie) ? $movie->duration : '') }}" placeholder="VD: 01:30:26" />
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Năm phát hành') }}</label>
                <input type="number" name="release_date" min="1900" class="form-control mb-3 mb-lg-0" value="{{ old('release_date', isset($movie) ? $movie->release_date : '') }}" />
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Phân loại độ tuổi') }}</label>
                <select name="rating_vn" class="form-select fw-bold" data-kt-select2="true" data-placeholder="{{ __('Chọn phân loại độ tuổi') }}" data-allow-clear="true" data-hide-search="true">
                    <option></option>
                    @foreach(\App\Models\Movie::$ratingLabel as $key => $value)
                        <option value="{{ $key }}" {{ isset($movie) && $movie->rating_vn == $key ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Cảnh báo nội dung') }}</label>
                <input type="text" name="alert" class="form-control mb-3 mb-lg-0" value="{{ old('alert', isset($movie) ? $movie->alert : '') }}" />
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Genre') }}</label>
                <input type="text" name="genre" class="form-control mb-3 mb-lg-0" value="{{ old('genre', isset($movie) ? $movie->genre : '') }}" />
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Director label') }}</label>
                <input type="text" name="director_label" class="form-control mb-3 mb-lg-0" value="{{ old('director_label', isset($movie) ? $movie->director_label : '') }}" />
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Director name') }}</label>
                <input type="text" name="director_name" class="form-control mb-3 mb-lg-0" value="{{ old('director_name', isset($movie) ? $movie->director_name : '') }}" />
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Diễn viên') }}</label>
                <select name="actor_ids[]" class="form-select fw-bold" data-kt-select2="true" data-placeholder="{{ __('Chọn diễn viên') }}" data-allow-clear="true" data-hide-search="true" multiple>
                    <option></option>
                    @foreach($actors as $key => $value)
                        <option value="{{ $key }}" {{ isset($movie) && is_array($movie->actor_ids) && in_array($key, $movie->actor_ids) ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-6 col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Available from') }}</label>
                <input name="available_from" class="form-control mb-3 mb-lg-0 datepicker" placeholder="{{ __('Chọn ngày') }}" value="{{ old('available_from', isset($movie) ? $movie->available_from : '') }}" />
            </div>

            <div class="col-md-6 col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Available to') }}</label>
                <input name="available_to" class="form-control mb-3 mb-lg-0 datepicker" placeholder="{{ __('Chọn ngày') }}" value="{{ old('available_to', isset($movie) ? $movie->available_to : '') }}" />
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Nhà cung cấp') }}</label>
                <select name="provider_id" class="form-select fw-bold select_provider" data-kt-select2="true" data-placeholder="{{ __('Chọn nhà cung cấp') }}" data-allow-clear="true" data-hide-search="true">
                    <option></option>
                    @foreach($providers as $key => $value)
                        <option value="{{ $key }}" {{ isset($movie) && $movie->provider_id == $key ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Gói dịch vụ') }}</label>
                <select name="package_id" class="form-select fw-bold" data-kt-select2="true" data-placeholder="{{ __('Chọn gói dịch vụ') }}" data-allow-clear="true" data-hide-search="true">
                    <option></option>
                    @foreach($packages as $key => $value)
                        <option value="{{ $key }}" {{ isset($movie) && $movie->package_id == $key ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>

@push('js')
    <script src="{{ asset('assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js') }}"></script>
    <script>
        initDropzoneUploadMultiFile('#dropzonejs_image_movie', function (response, file) {
            let html = `<div class="image-item">
                <div class="overlay-layer"></div>
                <div class="d-flex align-items-center gap-1 group-btn-image">
                    <a href="${response.url}" target="_blank" class="lh-1" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem ảnh') }}"><i class="ki-solid ki-eye fs-2 text-success"></i></a>
                    <i class="ki-duotone ki-trash-square fs-1 text-danger cursor-pointer btn-remove-image" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xóa') }}">
                        <span class="path1"></span>
                        <span class="path2"></span>
                        <span class="path3"></span>
                        <span class="path4"></span>
                    </i>
                </div>
                <input type="hidden" name="images[]" value="${response.url}">
                <img src="${response.url}" alt="{{ $movie->name ?? '' }}" class="w-md-100px w-90px border rounded shadow">
            </div>`
            $('.list-image').append(html)
        })

        ClassicEditor
            .create(document.querySelector('#kt_docs_ckeditor_classic'), {
                toolbar: ['undo', 'redo', '|', 'heading', '|', 'bold', 'italic', '|', 'blockQuote', 'bulletedList', 'numberedList', 'outdent', 'indent']
            })
            .then(editor => {
                description = editor
            })
            .catch(error => {
                console.error(error);
            });

        $('.select_provider').change(function() {
            $.ajax({
                url: '{{ route('package.getPackageByProviderId') }}',
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    provider_id: $(this).val(),
                },
                success: function (response) {
                    if (response.status) {
                        let options = '<option></option>'
                        response.packages.forEach(item => {
                            options += `<option value="${item.id}">${item.name}</option>`
                        })
                        $('select[name="package_id"]').html(options)
                    }
                }
            })
        })

        $(document).on('click', '.btn-remove-image', function () {
            Swal.fire({
                title: "{{ __('Bạn có chắc muốn xóa ảnh này không?') }}",
                icon: "question",
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "{{ __('Xóa') }}",
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $(this).closest('.image-item').remove()
                }
            })

        })
    </script>
@endpush
