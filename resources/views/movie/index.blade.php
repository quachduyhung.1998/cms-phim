@extends('layouts.app')
@section('title', __('Quản lý phim'))

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10 d-lg-none">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
                <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                    <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                        <h1 class="page-heading d-flex flex-column justify-content-center text-gray-900 fw-bold fs-3 m-0">@yield('title')</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('dashboard') }}" class="text-muted text-hover-primary">{{ __('Tổng quan') }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-500 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">@yield('title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <div id="kt_app_content_container" class="app-container container-fluid">
                <div class="card">
                    <!--begin::Card header-->
                    <div class="card-header border-0 pt-6">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <!--begin::Search-->
                            <form action="{{ route('movie.index') }}" class="d-lg-flex align-items-center gap-2">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <i class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></i>
                                    <input type="text" name="keyword" value="{{ request('keyword') }}" data-kt-user-table-filter="search" class="form-control w-lg-350px w-100 ps-13" placeholder="Từ khóa" />
                                </div>
                                <select name="status" class="form-select fw-bold w-lg-300px w-100 my-1" data-kt-select2="true" data-placeholder="{{ __('Trạng thái') }}" data-allow-clear="true" data-hide-search="true">
                                    <option></option>
                                    @foreach(\App\Models\Movie::$statusLabel as $key => $value)
                                        <option value="{{ $key }}" {{ request('status') == $key ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <button type="submit" class="btn btn-primary w-100 fw-semibold px-6 my-1">{{ __('Tìm kiếm') }}</button>
                            </form>
                            <!--end::Search-->
                        </div>
                        <!--begin::Card toolbar-->
                        <div class="card-toolbar">
                            <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                                <a href="{{ route('movie.create') }}" class="btn btn-primary">
                                    <i class="ki-outline ki-plus fs-2"></i>{{ __('Thêm phim') }}
                                </a>
                            </div>
                        </div>
                        <!--end::Card toolbar-->
                    </div>
                    <div class="card-body table-responsive py-4">
                        <table class="table align-middle table-row-dashed table-responsive fs-6 gy-5">
                            <thead>
                                <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                    <th class="text-center">{{ __('STT') }}</th>
                                    <th>{{ __('Ảnh') }}</th>
                                    <th>{{ __('Tên phim (tiếng Việt)') }}</th>
                                    <th>{{ __('Tên phim (tiếng Anh)') }}</th>
                                    <th>{{ __('Thể loại') }}</th>
                                    <th class="text-center">{{ __('Độ tuổi') }}</th>
                                    <th class="text-center">{{ __('Quốc gia') }}</th>
                                    <th class="text-center">{{ __('Năm phát hành') }}</th>
                                    <th>{{ __('Cảnh báo nội dung') }}</th>
                                    <th class="text-center">{{ __('Trạng thái') }}</th>
                                    <th class="text-end min-w-100px">{{ __('Thao tác') }}</th>
                                </tr>
                            </thead>
                            <tbody class="text-gray-600 fw-semibold">
                            @if ($movies->total() == 0)
                                <tr><td colspan="8">{{ __('Không có dữ liệu') }}</td></tr>
                            @else
                                @foreach($movies as $index => $movie)
                                    <tr>
                                        <td class="text-center">{{ ($movies->currentPage() - 1) * $movies->perPage() + $index+1 }}</td>
                                        <td><img src="{{ $movie->images && count($movie->images) ? asset($movie->images[0]) : '' }}" alt="" class="w-80px"></td>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <a href="{{ route('movie.edit', $movie->id) }}" class="text-gray-800 text-hover-primary mb-1">{{ $movie->name_vn }}</a>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <a href="{{ route('movie.edit', $movie->id) }}" class="text-gray-800 text-hover-primary mb-1">{{ $movie->name_en }}</a>
                                            </div>
                                        </td>
                                        <td>{{ $movie->category?->name }}</td>
                                        <td class="text-center">{{ $movie->rating_vn }}</td>
                                        <td class="text-center">{{ $movie->country }}</td>
                                        <td class="text-center">{{ $movie->release_date }}</td>
                                        <td>{{ $movie->alert }}</td>
                                        <td class="text-center">
                                            <div class="form-check form-switch d-inline-block">
                                                <input class="form-check-input change-status-movie" type="checkbox" role="switch" data-movie_id="{{ $movie->id }}" data-movie_name="{{ $movie->name_vn }}" {{ $movie->status === \App\Models\Movie::STATUS_ACTIVE ? 'checked' : '' }}>
                                            </div>
                                        </td>
                                        <td class="text-end">
                                            <a href="{{ route('movie.view', $movie->id) }}" class="btn btn-light btn-active-light-primary btn-sm">
                                                <i class="ki-solid ki-eye fs-3"></i>
                                                <span>{{ __('Chi tiết') }}</span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        {{ $movies->appends($_GET)->links('layouts.paginate') }}
                    </div>
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@push('js')
    <script>
        $('.change-status-movie').change(function() {
            const movie_id = $(this).data('movie_id')
            const movie_name = $(this).data('movie_name')
            const currentCheck = $(this).is(':checked')
            Swal.fire({
                title: '{{ __('Cập nhật trạng thái phim') }}',
                html: `{{ __('Bạn có chắc muốn cập nhật trạng thái phim') }} "${movie_name}"?`,
                icon: 'question',
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "{{ __('Cập nhật') }}",
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: 'btn btn-primary',
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ route('movie.change_status') }}',
                        type: 'PUT',
                        data: {
                            _token: '{{ csrf_token() }}',
                            movie_id
                        },
                        success: function (result) {
                            if (result.status) {
                                toastr.success("{{ __('Cập nhật trạng thái thành công') }}");
                            } else {
                                toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                            }
                        },
                        error: function (error) {
                            console.log(error)
                            toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                        }
                    }).always(function () {

                    })
                } else {
                    if (currentCheck) $(`input[data-movie_id="${movie_id}"]`).prop("checked", false)
                    else $(`input[data-movie_id="${movie_id}"]`).prop("checked", true)
                }
            })
        })
    </script>
@endpush
