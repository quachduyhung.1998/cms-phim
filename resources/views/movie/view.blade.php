@extends('layouts.app')
@section('title', __('Chi tiết phim'))

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10 d-lg-none">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
                <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                    <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                        <h1 class="page-heading d-flex flex-column justify-content-center text-gray-900 fw-bold fs-3 m-0">@yield('title')</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('dashboard') }}" class="text-muted text-hover-primary">{{ __('Tổng quan') }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-500 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('movie.index') }}" class="text-muted text-hover-primary">{{ __('Quản lý phim') }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-500 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">{{ $movie->name_vn }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <div id="kt_app_content_container" class="app-container container-fluid">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex align-items-center justify-content-between">
                            <h1 class="mb-3">{{ __('Thông tin tổng quan') }}</h1>
                            @if(auth()->user()->role === \App\Models\User::ROLE_BTV)
                                @if($userRequestUpdateMovie)
                                    @if($userRequestUpdateMovie->status === \App\Models\UserRequestUpdateMovie::STATUS_APPROVE)
                                        <a href="{{ route('movie.edit', $movie->id) }}" class="btn btn-light btn-active-light-primary">
                                            <i class="ki-solid ki-pencil fs-3"></i>
                                            <span>{{ __('Cập nhật') }}</span>
                                        </a>
                                    @elseif($userRequestUpdateMovie->status === \App\Models\UserRequestUpdateMovie::STATUS_REJECT)
                                        <button type="button" class="btn btn-light-danger" disabled>{{ __('Đã từ chối yêu cầu') }}</button>
                                    @else
                                        <button type="button" class="btn btn-light-warning" disabled>{{ __('Chờ phê duyệt') }}</button>
                                    @endif
                                @else
                                    <button type="button" class="btn btn-light-success btn-request-update-movie">{{ __('Yêu cầu cập nhật') }}</button>
                                @endif
                            @else
                                <a href="{{ route('movie.edit', $movie->id) }}" class="btn btn-light btn-active-light-primary">
                                    <i class="ki-solid ki-pencil fs-3"></i>
                                    <span>{{ __('Cập nhật') }}</span>
                                </a>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-lg-8 col-12">
                                <div class="d-flex align-items-end flex-wrap gap-3">
                                    @if($movie->images)
                                        @foreach($movie->images as $image)
                                            <img src="{{ asset($image) }}" class="w-200px rounded" />
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4 col-12 table-responsive">
                                <table class="table table-row-dashed">
                                    <tr>
                                        <td class="fw-semibold w-150px">{{ __('Phân loại độ tuổi') }}</td>
                                        <td>{{ $movie->rating_vn }}</td>
                                    </tr>
                                    <tr>
                                        <td class="fw-semibold w-150px">Genre</td>
                                        <td>{{ $movie->genre }}</td>
                                    </tr>
                                    <tr>
                                        <td class="fw-semibold w-150px">{{ __('Nhà cung cấp') }}</td>
                                        <td>{{ $movie->provider?->name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="fw-semibold w-150px">{{ __('Gói dịch vụ') }}</td>
                                        <td>{{ $movie->package?->name }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-12">
                                <div class="mt-5 mb-2 fw-bold fs-4">{{ __('Diễn viên') }}</div>
                                <div class="d-flex gap-3">
                                    @php
                                        $prev_color = $current_color = '';
                                    @endphp
                                    @foreach($actors as $actor)
                                        <div class="w-50px" title="{{ $actor->full_name }}">
                                            @if($actor->image)
                                                <img src="{{ asset($actor->image) }}" alt="" class="w-50px h-50px rounded-circle mb-2 object-fit-cover">
                                            @else
                                                @php
                                                    while ($current_color == $prev_color) {
                                                        $current_color = config('default.color')[rand(1, 5)];
                                                    }
                                                    $prev_color = $current_color;
                                                @endphp
                                                <div class="symbol symbol-circle symbol-50px overflow-hidden">
                                                    <div class="symbol-label fs-3 bg-light-{{ $current_color }} text-{{ $current_color }}">{{ strtoupper(mb_substr(last(explode(' ', $actor->full_name)), 0, 1, 'UTF-8')) }}</div>
                                                </div>
                                            @endif
                                            <div class="text-limit-1-line">{{ $actor->full_name }}</div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive mt-8">
                            <table class="table table-row-dashed">
                                <tr>
                                    <td class="fw-semibold w-200px">{{ __('Tên phim tiếng Việt') }}</td>
                                    <td>{{ $movie->title_vn }}</td>
                                </tr>
                                <tr>
                                    <td class="fw-semibold w-200px">{{ __('Tên phim tiếng Anh') }}</td>
                                    <td>{{ $movie->title_en }}</td>
                                </tr>
                                <tr>
                                    <td class="fw-semibold w-200px">{{ __('Tóm tắt') }}</td>
                                    <td>{!! $movie->vn_summary !!}</td>
                                </tr>
                                <tr>
                                    <td class="fw-semibold w-200px">{{ __('Tên tập phim') }}</td>
                                    <td>{{ $movie->practice }}</td>
                                </tr>
                                <tr>
                                    <td class="fw-semibold w-200px">{{ __('Quốc gia') }}</td>
                                    <td>{{ $movie->country }}</td>
                                </tr>
                                <tr>
                                    <td class="fw-semibold w-200px">{{ __('Năm phát hành') }}</td>
                                    <td>{{ $movie->release_date }}</td>
                                </tr>
                                <tr>
                                    <td class="fw-semibold w-200px">{{ __('Thể loại') }}</td>
                                    <td>{{ $movie->category?->name }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@push('js')
    <script>
        $('.btn-request-update-movie').click(function() {
            Swal.fire({
                title: "{{ __('Yêu cầu cập nhật') }}",
                html: "{{ __('Yêu cầu sẽ được gửi cho Admin, bạn hãy chờ để được phê duyệt') }}",
                icon: "question",
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "{{ __('Gửi yêu cầu') }}",
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ route('movie.request_update') }}',
                        type: 'POST',
                        data: {
                            _token: '{{ csrf_token() }}',
                            movie_id: '{{ $movie->id }}'
                        },
                        beforeSend: KTApp.showPageLoading(),
                        success: function (response) {
                            if (response.status) {
                                toastr.success("{{ __('Gửi yêu cầu thành công') }}");
                                $('.btn-request-update-movie')
                                    .html('{{ __('Chờ phê duyệt') }}')
                                    .removeClass('btn-light-success')
                                    .addClass('btn-light-warning')
                                    .attr('disabled', true)
                            } else {
                                toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                            }
                        }
                    }).always(function () {
                        KTApp.hidePageLoading()
                    })
                }
            })
        })
    </script>
@endpush
