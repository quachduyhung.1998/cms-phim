@extends('layouts._blank')
@section('title', __('Thiết lập mật khẩu'))

@push('css')
    <style>
        body {
            background-image: url('{{ asset('assets/media/auth/bg4.jpg') }}');
        }
        [data-bs-theme="dark"] body {
            background-image: url('{{ asset('assets/media/auth/bg4-dark.jpg') }}');
        }
    </style>
@endpush

@section('content')
    <div class="d-flex flex-column flex-column-fluid flex-lg-row">
        <div class="d-flex flex-center w-lg-50 pt-15 pt-lg-0 px-10">
            <div class="d-flex flex-center flex-lg-start flex-column">
                <span class="mb-7">
                    <img alt="Logo" src="{{ asset('assets/media/logos/ttbqs-logo.svg') }}" />
                </span>
            </div>
        </div>
        <div class="d-flex flex-column-fluid flex-lg-row-auto justify-content-center justify-content-lg-end p-12 p-lg-20">
            <div class="bg-body d-flex flex-column align-items-stretch flex-center rounded-4 w-md-600px p-20">
                <div class="d-flex flex-center flex-column flex-column-fluid px-lg-10 pb-15 pb-lg-20">
                    <form class="form w-100" novalidate="novalidate" id="kt_password_reset_form" action="{{ route('password.update') }}" method="POST">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="text-center mb-10">
                            <h1 class="text-gray-900 fw-bolder mb-3">{{ __('Thiết lập mật khẩu') }}</h1>
                        </div>
                        <div class="fv-row mb-8">
                            <label class="fw-bold mb-1">{{ __('Email') }}</label>
                            <input type="email" name="email" autocomplete="off" class="form-control bg-transparent" value="{{ request('email') }}" />
                            @error('email')
                                <span class="text-danger d-block mt-1">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="fv-row mb-8">
                            <label class="fw-bold mb-1">{{ __('Mật khẩu') }}</label>
                            <input type="password" name="password" autocomplete="off" class="form-control bg-transparent" />
                            @error('password')
                                <span class="text-danger d-block mt-1">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="fv-row mb-8">
                            <label class="fw-bold mb-1">{{ __('Nhập lại mật khẩu') }}</label>
                            <input type="password" name="password_confirmation" autocomplete="off" class="form-control bg-transparent" />
                            @error('password_confirmation')
                                <span class="text-danger d-block mt-1">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="d-flex flex-wrap justify-content-center pb-lg-0">
                            <button type="submit" id="kt_password_update_submit" class="btn btn-primary me-4">
                                <span class="indicator-label">{{ __('Thiết lập ngay') }}</span>
                                <span class="indicator-progress">{{ __('Đang gửi') }}...<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                            <a href="{{ route('login') }}" class="btn btn-light">{{ __('Trở lại') }}</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $('#kt_password_update_submit').click(function () {
            $('.indicator-label').css('display', 'none')
            $('.indicator-progress').css('display', 'block')
        })
    </script>
@endpush

