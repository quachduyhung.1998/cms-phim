@extends('layouts._blank')
@section('title', __('Quên mật khẩu'))

@push('css')
    <style>
        body {
            background-image: url('{{ asset('assets/media/auth/bg4.jpg') }}');
        }
        [data-bs-theme="dark"] body {
            background-image: url('{{ asset('assets/media/auth/bg4-dark.jpg') }}');
        }
    </style>
@endpush

@section('content')
    <div class="d-flex flex-column flex-column-fluid flex-lg-row">
        <div class="d-flex flex-center w-lg-50 pt-15 pt-lg-0 px-10">
            <div class="d-flex flex-center flex-lg-start flex-column">
                <span class="mb-7">
                    <img alt="Logo" src="{{ asset('assets/media/logos/ttbqs-logo.svg') }}" />
                </span>
            </div>
        </div>
        <div class="d-flex flex-column-fluid flex-lg-row-auto justify-content-center justify-content-lg-end p-12 p-lg-20">
            <div class="bg-body d-flex flex-column align-items-stretch flex-center rounded-4 w-md-600px p-20">
                <div class="d-flex flex-center flex-column flex-column-fluid px-lg-10 pb-15 pb-lg-20">
                    <div id="kt_password_reset_submit_success" class="text-primary text-center fs-2 d-none">
                        <p>{{ __('Yêu cầu thành công, vui lòng kiểm tra email của bạn để thiết lập lại mật khẩu') }}</p>
                        <a href="{{ url()->previous() }}" class="btn btn-primary">{{ __('Trở về') }}</a>
                    </div>
                    <form class="form w-100" novalidate="novalidate" id="kt_password_reset_form">
                        @csrf
                        <div class="text-center mb-10">
                            <h1 class="text-gray-900 fw-bolder mb-3">{{ __('Quên mật khẩu') }} ?</h1>
                            <div class="text-gray-500 fw-semibold fs-6">{{ __('Nhập email của bạn để thiết lập lại mật khẩu.') }}</div>
                        </div>
                        <div class="fv-row mb-8">
                            <input type="email" placeholder="Nhập email của bạn vào đây" name="email" autocomplete="off" class="form-control bg-transparent" />
                            <span class="text-danger d-block mt-1 box-err"></span>
                        </div>
                        <div class="d-flex flex-wrap justify-content-center pb-lg-0">
                            <button type="button" id="kt_password_reset_submit" class="btn btn-primary me-4">
                                <span class="indicator-label">{{ __('Gửi') }}</span>
                                <span class="indicator-progress">{{ __('Đang gửi') }}...<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                            <a href="{{ route('login') }}" class="btn btn-light">{{ __('Trở lại') }}</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $('#kt_password_reset_submit').click(function () {
            let _token = $('input[name="_token"]').val()
            let email = $('input[name="email"]').val()
            $('.indicator-label').css('display', 'none')
            $('.indicator-progress').css('display', 'block')
            $(this).attr('disabled', true)
            $.ajax({
                url: '{{ route('password.email') }}',
                type: 'POST',
                data: {
                    _token,
                    email
                },
                success: function (result) {
                    if (result.status) {
                        $('#kt_password_reset_submit_success').removeClass('d-none')
                        $('#kt_password_reset_form').hide()
                    } else {
                        $('.box-err').html(result.message)
                        $('input[name="email"]').addClass('border-danger')
                    }
                },
                error: function(error) {
                    $('.box-err').html(error.responseJSON.message)
                    $('input[name="email"]').addClass('border-danger')
                }
            }).always(function () {
                $('.indicator-label').removeAttr('style')
                $('.indicator-progress').removeAttr('style')
                $('#kt_password_reset_submit').attr('disabled', false)
            })
        })
    </script>
@endpush

