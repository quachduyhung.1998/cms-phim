@extends('layouts._blank')
@section('title', __('Đăng nhập'))

@push('css')
    <style>
        body {
            background-image: url('{{ asset('assets/media/auth/bg4.jpg') }}');
        }
        [data-bs-theme="dark"] body {
            background-image: url('{{ asset('assets/media/auth/bg4-dark.jpg') }}');
        }
    </style>
@endpush

@section('content')
    <div class="d-flex flex-column flex-column-fluid flex-lg-row">
        <div class="d-flex flex-center w-lg-50 pt-15 pt-lg-0 px-10">
            <div class="d-flex flex-center flex-lg-start flex-column">
                <span class="mb-7">
                    <img alt="Logo" src="{{ asset('assets/media/logos/ttbqs-logo.svg') }}" />
                </span>
            </div>
        </div>
        <div class="d-flex flex-column-fluid flex-lg-row-auto justify-content-center justify-content-lg-end p-12 p-lg-20">
            <div class="bg-body d-flex flex-column align-items-stretch flex-center rounded-4 w-md-600px p-20">
                <div class="d-flex flex-center flex-column flex-column-fluid px-lg-10 pb-15 pb-lg-20">
                    <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" action="" method="POST">
                        @csrf
                        <div class="text-center mb-11">
                            <h1 class="text-gray-900 fw-bolder fs-3x mb-3">{{ __('Đăng nhập') }}</h1>
                        </div>
                        <div class="fv-row mb-8">
                            <label class="fw-bold mb-1">{{ __('Tên đăng nhập') }}</label>
                            <input type="text" name="username" autocomplete="off" class="form-control bg-transparent" value="{{ old('username') }}" />
                            @error('username')
                                <span class="text-danger d-block mt-1">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="fv-row mb-3">
                            <label class="fw-bold mb-1">{{ __('Mật khẩu') }}</label>
                            <input type="password" name="password" autocomplete="off" class="form-control bg-transparent @error('password') border-danger @enderror" />
                            @error('password')
                                <span class="text-danger d-block mt-1">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="d-flex justify-content-end flex-wrap gap-3 fs-base fw-semibold mb-8">
                            <a href="{{ route('password.request') }}" class="link-primary">{{ __('Quên mật khẩu') }} ?</a>
                        </div>
                        <div class="d-grid mb-10">
                            <button type="submit" id="kt_sign_in_submit" class="btn btn-primary">
                                <span class="indicator-label">{{ __('Đăng nhập') }}</span>
                                <span class="indicator-progress">{{ __('Đang đăng nhập') }}...<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        @if(Session::has('status_reset_password'))
            Swal.fire({
                text: "{{ \Illuminate\Support\Facades\Session::get('status_reset_password') }}",
                icon: "success",
                buttonsStyling: false,
                confirmButtonText: "Đăng nhập ngay",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            })
        @endif

        $('#kt_sign_in_submit').click(function () {
            $('.indicator-label').css('display', 'none')
            $('.indicator-progress').css('display', 'block')
        })
    </script>
@endpush
