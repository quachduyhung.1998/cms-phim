@php
    $prev_color = $current_color = '';
    $prev_user_id = '';
@endphp
@foreach($notifications as $notification)
    @php
        while ($current_color == $prev_color && $notification->user_id !== $prev_user_id) {
            $current_color = config('default.color')[rand(1, 5)];
            $prev_user_id = $notification->user_id;
        }
        $prev_color = $current_color;

        $html = '';
        if ($notification->model_type == \App\Models\Notification::MODEL_TYPE_USER_REQUEST_UPDATE_MOVIE) {
            $html = '<b>'.$notification->user?->full_name.'</b> yêu cầu cấp quyền cập nhật phim <b>"'.$notification->userRequestUpdateMovie->movie?->name_vn.'"</b>';
        } elseif ($notification->model_type == \App\Models\Notification::MODEL_TYPE_MOVIE) {
            if ($notification->status_cotnent == \App\Models\UserRequestUpdateMovie::STATUS_APPROVE) {
                $html = '<b>'.$notification->user?->full_name.'</b> đã <span class="text-primary">phê duyệt</span> yêu cầu cập nhật phim <b>"'.$notification->movie?->name_vn.'"</b> của bạn';
            } elseif ($notification->status_cotnent == \App\Models\UserRequestUpdateMovie::STATUS_REJECT) {
                $html = '<b>'.$notification->user?->full_name.'</b> đã <span class="text-danger">chối yêu</span> yêu cầu cập nhật phim <b>"'.$notification->movie?->name_vn.'"</b> của bạn';
            }
        }

        if ($notification->content) {
            $html .= ': ' . str_replace(['<p>', '</p>'], ['', ''], $notification->content);
        }
    @endphp
    <a href="{{ route('notification.click', $notification->id) }}" class="position-relative d-flex flex-stack bg-hover-light-secondary rounded p-4 header-notification-item" data-notification-id="{{ $notification->id }}">
        <span class="position-absolute top-5"></span>
        <div class="d-flex align-items-center">
            <div class="symbol symbol-50px me-4">
                <div class="symbol-label rounded-circle fs-3 bg-light-{{ $current_color }} text-{{ $current_color }}">{{ strtoupper(mb_substr(last(explode(' ', $notification->user?->full_name)), 0, 1, 'UTF-8')) }}</div>
            </div>
            <div class="mb-0 me-2">
                <div class="fs-6 text-gray-800 notification-item-content">
                    {!! $html !!}
                </div>
                <span class="fs-8 {{ $notification->notificationUsers[0]->status === \App\Models\NotificationUser::STATUS_READ ? 'text-gray-500' : '' }}">{{ $notification->created_at->diffForHumans() }}</span>
            </div>
        </div>
    </a>
@endforeach
