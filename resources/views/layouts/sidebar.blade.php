<div id="kt_app_sidebar" class="app-sidebar flex-column" data-kt-drawer="true" data-kt-drawer-name="app-sidebar"
     data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="250px"
     data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_app_sidebar_mobile_toggle">
    <div class="app-sidebar-header d-flex flex-stack d-none d-lg-flex pt-8 pb-2" id="kt_app_sidebar_header">
        <!--begin::Logo-->
        <a href="{{ route('dashboard') }}" class="app-sidebar-logo">
            <img alt="Logo" src="{{ asset('assets/media/logos/ttbqs-logo.svg') }}"
                 class="h-40px d-none d-sm-inline app-sidebar-logo-default theme-light-show"/>
            <img alt="Logo" src="{{ asset('assets/media/logos/ttbqs-logo.svg') }}"
                 class="h-20px h-lg-25px theme-dark-show"/>
        </a>
        <!--end::Logo-->
        <!--begin::Sidebar toggle-->
        <div id="kt_app_sidebar_toggle"
             class="app-sidebar-toggle btn btn-sm btn-icon bg-light btn-color-gray-700 btn-active-color-primary d-none d-lg-flex rotate"
             data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body"
             data-kt-toggle-name="app-sidebar-minimize">
            <i class="ki-outline ki-text-align-right rotate-180 fs-1"></i>
        </div>
        <!--end::Sidebar toggle-->
    </div>
    <!--begin::Navs-->
    <div class="app-sidebar-navs flex-column-fluid py-6" id="kt_app_sidebar_navs">
        <div id="kt_app_sidebar_navs_wrappers" class="app-sidebar-wrapper hover-scroll-y my-2" data-kt-scroll="true"
             data-kt-scroll-activate="true" data-kt-scroll-height="auto"
             data-kt-scroll-dependencies="#kt_app_sidebar_header" data-kt-scroll-wrappers="#kt_app_sidebar_navs"
             data-kt-scroll-offset="5px">
            <div id="#kt_app_sidebar_menu" data-kt-menu="true" data-kt-menu-expand="false"
                 class="app-sidebar-menu-primary menu menu-column menu-rounded menu-sub-indention menu-state-bullet-primary">
                <div class="menu-item mb-2 d-block d-lg-none">
                    <div class="menu-heading text-uppercase fs-7 fw-bold">Menu</div>
                    <div class="app-sidebar-separator separator"></div>
                </div>
{{--                <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'dashboard') ? 'here show' : '' }}">--}}
{{--                    <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'dashboard') ? 'active' : '' }}" href="{{ route('dashboard') }}">--}}
{{--                        <span class="menu-icon">--}}
{{--                            <i class="ki-outline ki-home-2 fs-2"></i>--}}
{{--                        </span>--}}
{{--                        <span class="menu-title">{{ __('Tổng quan') }}</span>--}}
{{--                    </a>--}}
{{--                </div>--}}
                @can('user.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'user.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'user.') ? 'active' : '' }}" href="{{ route('user.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-people fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Tài khoản') }}</span>
                        </a>
                    </div>
                @endcan
                @can('category.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'category.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'category.') ? 'active' : '' }}" href="{{ route('category.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-tablet-text-up fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Danh mục') }}</span>
                        </a>
                    </div>
                @endcan
                @can('provider.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'provider.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'provider.') ? 'active' : '' }}" href="{{ route('provider.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-abstract-26 fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Nhà cung cấp') }}</span>
                        </a>
                    </div>
                @endcan
                @can('package.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'package.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'package.') ? 'active' : '' }}" href="{{ route('package.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-security-user fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Gói dịch vụ') }}</span>
                        </a>
                    </div>
                @endcan
                @can('actor.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'actor.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'actor.') ? 'active' : '' }}" href="{{ route('actor.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-people fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Diễn viên') }}</span>
                        </a>
                    </div>
                @endcan
                @can('movie.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'movie.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'movie.') ? 'active' : '' }}" href="{{ route('movie.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-to-right fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Quản lý phim') }}</span>
                        </a>
                    </div>
                @endcan
                @can('user_request_update_movie.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'user_request_update_movies.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'user_request_update_movies.') ? 'active' : '' }}" href="{{ route('user_request_update_movies.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-question fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Yêu cầu cập nhật phim') }}</span>
                        </a>
                    </div>
                @endcan
            </div>
        </div>
    </div>
    <!--end::Navs-->
</div>
