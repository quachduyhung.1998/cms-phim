<?php

namespace App\Services\Notification;

use App\Helpers\NotificationFirebaseHelper;
use App\Mail\ForgotPassword;
use App\Models\Notification;
use App\Models\NotificationUser;
use App\Models\User;
use App\Models\UserFirebaseToken;
use App\Models\UserRequestUpdateMovie;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class NotificationServiceImp implements NotificationService
{
    public function create($userId, $sendToUserIds, $modelType, $modelId, $notificationContent = null, $statusContent = null): bool
    {
        $model = App::make($modelType)->find($modelId);
        if ($model) {
            $notification = Notification::create([
                'user_id' => $userId,
                'model_type' => $modelType,
                'model_id' => $modelId,
                'content' => $notificationContent,
                'status_content' => $statusContent
            ]);
            if ($notification) {
                $tokens = [];
                $dataInsert = [];
                foreach ($sendToUserIds as $sendToUserId) {
                    $dataInsert[] = [
                        'notification_id' => $notification->id,
                        'user_id' => $sendToUserId,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ];
                }

                if (!empty($dataInsert)) {
                    NotificationUser::insert($dataInsert);
                    $tokens = UserFirebaseToken::query()->whereIn('user_id', $sendToUserIds)->pluck('token')->toArray();
                }

                if (!empty($tokens)) {
                    $body = '';
                    $html = '';
                    if ($modelType == Notification::MODEL_TYPE_USER_REQUEST_UPDATE_MOVIE) {
                        $body = $notification->user?->full_name.' yêu cầu cập nhật phim "'.$model->movie?->name_vn.'"';
                        $html = '<b>'.$notification->user?->full_name.'</b> yêu cầu cấp quyền cập nhật phim <b>"'.$model->movie?->name_vn.'"</b>';
                    } elseif ($modelType == Notification::MODEL_TYPE_MOVIE) {
                        if ($statusContent == UserRequestUpdateMovie::STATUS_APPROVE) {
                            $body = $notification->user?->full_name.' đã phê duyệt yêu cầu cập nhật phim "'.$model->movie?->name_vn.'" của bạn';
                            $html = '<b>'.$notification->user?->full_name.'</b> đã <span class="text-primary">phê duyệt</span> yêu cầu cập nhật phim <b>"'.$model->movie?->name_vn.'"</b> của bạn';
                        } elseif ($statusContent == UserRequestUpdateMovie::STATUS_REJECT) {
                            $body = $notification->user?->full_name.' đã từ chối yêu cầu cập nhật phim "'.$model->movie?->name_vn.'" của bạn';
                            $html = '<b>'.$notification->user?->full_name.'</b> đã từ <span class="text-danger">chối yêu</span> cầu cập nhật phim <b>"'.$model->movie?->name_vn.'"</b> của bạn';
                        }
                    }

                    NotificationFirebaseHelper::send(
                        tokens: $tokens,
                        body: $body,
                        click_action: route('notification.click', $notification->id),
                        data: [
                            'notification_id' => $notification->id,
                            'html' => $html
                        ]
                    );
                }

                return true;
            }
        }
        return false;
    }

    public function getAllByUserId($userId): array
    {
        $totalNotification = NotificationUser::query()->where('user_id', $userId)->count();

        $hasNewNotification = false;
        $countNewNotification = NotificationUser::query()
            ->where('user_id', $userId)
            ->where('is_new', NotificationUser::IS_NEW)
            ->count();
        if ($countNewNotification) $hasNewNotification = true;

        $totalNotificationUnread = NotificationUser::query()
            ->where('user_id', $userId)
            ->where('status', NotificationUser::STATUS_UNREAD)
            ->count();

        $notificationIds = NotificationUser::query()->where('user_id', $userId)->pluck('notification_id')->toArray();
        $notifications = Notification::with([
                'user',
                'movie',
                'userRequestUpdateMovie',
                'notificationUsers' => function ($q) use ($userId) {
                    $q->where('user_id', $userId);
                }
            ])
            ->whereIn('id', $notificationIds)
            ->latest('id')
            ->paginate(10);

        return [
            'totalNotification' => $totalNotification,
            'hasNewNotification' => $hasNewNotification,
            'totalNotificationUnread' => $totalNotificationUnread,
            'notifications' => $notifications,
        ];
    }
}
