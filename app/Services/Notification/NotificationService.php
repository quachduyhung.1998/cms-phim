<?php

namespace App\Services\Notification;

interface NotificationService
{
    public function create($userId, $sendToUserIds, $modelType, $modelId, $notificationContent = null, $statusContent = null);

    public function getAllByUserId($userId);
}
