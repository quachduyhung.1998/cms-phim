<?php

namespace App\Helpers;

class NotificationFirebaseHelper
{
    public static function send(array $tokens, $body, $click_action, $title = null, $data = [])
    {
        $url ="https://fcm.googleapis.com/fcm/send";

        $fields = array(
            'registration_ids' => array_unique($tokens),
            "notification" => [
                "title" => $title ?? config('app.name'),
                "body" => $body,
                "icon" => asset('assets/media/logos/favicon.png'),
                "click_action" => $click_action,
                "data" => json_encode($data),
                "sound" => "default"
            ]
        );

        $headers = [
            'Content-Type:application/json',
            'Authorization: key=AAAAcO0-zno:APA91bFZewsmqOhI0WKpuDsTPPgib7kLDTGMVlZXXDnINSoxLre1a4lHsgnrZMvWwp1bMKAuH-RMhGJ9XkME1XrFgPGivHh5ML8DBbe5wDUqxuwIs1ljrfXiA2JXUJU9iXgW1888kI7C'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_exec($ch);
        curl_close($ch);
    }
}
