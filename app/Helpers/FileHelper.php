<?php

namespace App\Helpers;

use Illuminate\Support\Str;

class FileHelper
{
    public static function upload($file): string
    {
        $extension = $file->getClientOriginalExtension();
        $fileName = date('Y_m_d_His_');
        $fileName .= Str::slug(str_replace("." . $extension, "", $file->getClientOriginalName()));
        $fileName .= "_" . md5(time()) . "." . $extension;
        $save = $file->storeAs('uploads', $fileName, 'public');
        return "/storage/$save";
    }
}
