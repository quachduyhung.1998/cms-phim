<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserRequestUpdateMovie extends Model
{
    use HasFactory, SoftDeletes;

    protected  $fillable = [
        "user_id",
        "movie_id",
        "admin_id",
        "status",
    ];

    const STATUS_INIT = 0;
    const STATUS_APPROVE = 1;
    const STATUS_REJECT = 2;
    public static array $statusLabel = [
        self::STATUS_INIT => 'Chờ phê duyệt',
        self::STATUS_APPROVE => 'Phê duyệt',
        self::STATUS_REJECT => 'Từ chối',
    ];
    public static array $statusColor = [
        self::STATUS_INIT => 'warning',
        self::STATUS_APPROVE => 'primary',
        self::STATUS_REJECT => 'danger',
    ];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function admin(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'admin_id', 'id');
    }

    public function movie(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Movie::class);
    }
}
