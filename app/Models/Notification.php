<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use HasFactory, SoftDeletes;

    protected  $fillable = [
        "user_id",
        "model_type",
        "model_id",
        "content",
        "status_content",
    ];

    const MODEL_TYPE_MOVIE = 'App\Models\Movie';
    const MODEL_TYPE_USER_REQUEST_UPDATE_MOVIE = 'App\Models\UserRequestUpdateMovie';

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function movie(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Movie::class, 'model_id', 'id');
    }

    public function userRequestUpdateMovie(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(UserRequestUpdateMovie::class, 'model_id', 'id');
    }

    public function notificationUsers(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(NotificationUser::class);
    }
}
