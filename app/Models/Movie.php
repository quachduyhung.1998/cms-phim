<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name_vn',
        'name_en',
        'images',
        'practice',
        'trailer',
        'intro_start',
        'intro_end',
        'outro',
        'category_id',
        'country',
        'vn_summary',
        'duration',
        'release_date',
        'rating',
        'rating_vn',
        'alert',
        'genre',
        'director_label',
        'director_name',
        'actor_ids',
        'available_from',
        'available_to',
        'provider_id',
        'package_id',
        'status',
    ];

    protected $casts = [
        'available_from' => 'datetime',
        'available_to' => 'datetime',
        'images' => 'array',
        'actor_ids' => 'array',
    ];

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    public static array $statusLabel = [
        self::STATUS_ACTIVE => 'Đang hoạt động',
        self::STATUS_INACTIVE => 'Không hoạt động'
    ];
    public static array $ratingLabel = [
        'P' => 'P',
        'K' => 'K',
        'T13' => 'T13',
        'T16' => 'T16',
        'T18' => 'T18',
        'C' => 'C',
    ];

    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function provider(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Provider::class);
    }

    public function package(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Package::class);
    }
}
