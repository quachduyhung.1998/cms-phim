<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\CreateMemberRequest;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateMemberRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\Category;
use App\Models\Package;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::query()
            ->where('id', '!=', auth()->user()->id)
            ->where('role', '!=', User::ROLE_SUPER_ADMIN)
            ->when($request->keyword, function ($q) use ($request) {
                $q->where('full_name', 'like', "%$request->keyword%")
                    ->orWhere('email', 'like', "%$request->keyword%")
                    ->orWhere('phone', 'like', "%$request->keyword%");
            })
            ->when($request->role_search, function ($q) use ($request) {
                $q->where('role', $request->role_search);
            })
            ->when($request->status, function ($q) use ($request) {
                $q->where('status', $request->status);
            })
            ->latest('id')
            ->paginate(10);
        return view('user.index', compact('users'));
    }

    public function store(CreateUserRequest $request)
    {
        $user = User::create($request->except('_token'));
        if ($user) {
            $role = Role::find($request->role);
            if ($role) $user->assignRole($role);
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('user.modal_update', compact('user'));
    }

    public function update(UpdateUserRequest $request)
    {
        $user = User::find($request->id);
        $roleCurrentId = $user->role;
        if ($user) {
            if ($roleCurrentId != $request->role) {
                $roleCurrent = Role::find($roleCurrentId);
                if ($roleCurrent) $user->removeRole($roleCurrent);

                $roleNew = Role::find($request->role);
                if ($roleNew) $user->assignRole($roleNew);
            }
            $user->update($request->except('_token'));
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function changeStatus(Request $request)
    {
        $user = User::find($request->user_id);
        if ($user) {
            $user->status = $user->status === User::STATUS_ACTIVE ? User::STATUS_INACTIVE : User::STATUS_ACTIVE;
            $user->save();
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function resetPassword(Request $request)
    {
        $user = User::find($request->user_id);
        if ($user) {
            $user->password = Hash::make(config('default.password_default'));
            $user->save();
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function delete(Request $request)
    {
        $user = User::find($request->user_id);
        if ($user) {
            $user->delete();
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }
}
