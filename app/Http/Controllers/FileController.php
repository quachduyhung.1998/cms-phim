<?php

namespace App\Http\Controllers;

use App\Helpers\FileHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class FileController extends Controller
{
    public function upload(Request $request)
    {
        if (!$request->file) return response()->json(['status' => false]);
        $filePath = null;
        if (!empty($request->file)) {
            $filePath = FileHelper::upload($request->file);
        }
        return response()->json(['url' => $filePath]);
    }
}
