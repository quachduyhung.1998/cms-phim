<?php

namespace App\Http\Controllers;

use App\Models\UserRequestUpdateMovie;
use Illuminate\Http\Request;

class UserRequestUpdateMovieController extends Controller
{
    public function index(Request $request)
    {
        $userRequestUpdateMovies = UserRequestUpdateMovie::query()
            ->latest('id')
            ->paginate(10);
        return view('user_request_update_movie.index', compact('userRequestUpdateMovies'));
    }

    public function view($id)
    {
        $userRequestUpdateMovie = UserRequestUpdateMovie::find($id);
        if (!$userRequestUpdateMovie) abort(404);
        return view('user_request_update_movie.view', compact('userRequestUpdateMovie'));
    }

    public function update(Request $request)
    {
        $actor = UserRequestUpdateMovie::find($request->id);
        if ($actor) {
            $actor->update(array_merge($request->except('_token'), ['admin_id' => auth()->user()->id]));
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }
}
