<?php

namespace App\Http\Controllers;

use App\Http\Requests\Actor\CreateActorRequest;
use App\Http\Requests\Actor\UpdateActorRequest;
use App\Models\Actor;
use Illuminate\Http\Request;

class ActorController extends Controller
{
    public function index(Request $request)
    {
        $actors = Actor::query()
            ->when($request->keyword, function ($q) use ($request) {
                $q->where('name', 'like', "%$request->keyword%")
                    ->orWhere('description', 'like', "%$request->keyword%");
            })
            ->latest('id')
            ->paginate(10);
        return view('actor.index', compact('actors'));
    }

    public function store(CreateActorRequest $request)
    {
        $actor = Actor::create($request->except('_token'));
        if ($actor) {
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function edit($id)
    {
        $actor = Actor::find($id);
        return view('actor.modal_update', compact('actor'));
    }

    public function update(UpdateActorRequest $request)
    {
        $actor = Actor::find($request->id);
        if ($actor) {
            $actor->update($request->except('_token'));
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function delete(Request $request)
    {
        $actor = Actor::find($request->id);
        if ($actor) {
            $actor->delete();
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }
}
