<?php

namespace App\Http\Controllers;

use App\Http\Requests\Provider\CreateProviderRequest;
use App\Http\Requests\Provider\UpdateProviderRequest;
use App\Models\Provider;
use Illuminate\Http\Request;

class ProviderController extends Controller
{
    public function index(Request $request)
    {
        $providers = Provider::query()
            ->when($request->keyword, function ($q) use ($request) {
                $q->where('name', 'like', "%$request->keyword%")
                    ->orWhere('description', 'like', "%$request->keyword%");
            })
            ->latest('id')
            ->paginate(10);
        return view('provider.index', compact('providers'));
    }

    public function store(CreateProviderRequest $request)
    {
        $provider = Provider::create($request->except('_token'));
        if ($provider) {
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function edit($id)
    {
        $provider = Provider::find($id);
        return view('provider.modal_update', compact('provider'));
    }

    public function update(UpdateProviderRequest $request)
    {
        $provider = Provider::find($request->id);
        if ($provider) {
            $provider->update($request->except('_token'));
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function delete(Request $request)
    {
        $provider = Provider::find($request->id);
        if ($provider) {
            $provider->delete();
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);

    }
}
