<?php

namespace App\Http\Controllers;

use App\Http\Requests\Movie\CreateMovieRequest;
use App\Http\Requests\Movie\UpdateMovieRequest;
use App\Models\Actor;
use App\Models\Category;
use App\Models\Movie;
use App\Models\Notification;
use App\Models\Package;
use App\Models\Provider;
use App\Models\UserRequestUpdateMovie;
use App\Services\Notification\NotificationService;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    public function __construct(protected NotificationService $notificationService) {}

    public function index(Request $request)
    {
        $movies = Movie::query()
            ->when($request->keyword, function ($q) use ($request) {
                $q->where(function($q2) use ($request) {
                    $q2->where('name_vn', 'like', "%$request->keyword%")
                        ->orWhere('name_en', 'like', "%$request->keyword%");
                });
            })
            ->when($request->status, function ($q) use ($request) {
                $q->where('status', $request->status);
            })
            ->paginate(10);
        return view('movie.index', compact('movies'));
    }

    public function view($id)
    {
        $movie = Movie::find($id);
        $actors = Actor::query()->whereIn('id', $movie->actor_ids ?? [])->get();
        $userRequestUpdateMovie = UserRequestUpdateMovie::query()
            ->where('user_id', auth()->user()->id)
            ->where('movie_id', $id)
            ->first();
        return view('movie.view', compact('movie', 'actors', 'userRequestUpdateMovie'));
    }

    public function create()
    {
        $categories = Category::query()->pluck('name', 'id')->toArray();
        $actors = Actor::query()->pluck('full_name', 'id')->toArray();
        $providers = Provider::query()->pluck('name', 'id')->toArray();
        $packages = Package::query()->pluck('name', 'id')->toArray();
        return view('movie.create', compact('categories', 'actors', 'providers', 'packages'));
    }

    public function store(CreateMovieRequest $request)
    {
        $movie = Movie::create($request->except('_token'));
        if ($movie) {
            return redirect()->route('movie.index')->with('success', __('Thêm phim thành công'));
        }
        return back()->with('error', __('Đã có lỗi xảy ra, vui lòng thử lại sau'));
    }

    public function edit($id)
    {
        $movie = Movie::find($id);
        $categories = Category::query()->pluck('name', 'id')->toArray();
        $actors = Actor::query()->pluck('full_name', 'id')->toArray();
        $providers = Provider::query()->pluck('name', 'id')->toArray();
        $packages = Package::query()->pluck('name', 'id')->toArray();
        return view('movie.edit', compact('movie', 'categories', 'actors', 'providers', 'packages'));
    }

    public function update(UpdateMovieRequest $request)
    {
        $movie = Movie::find($request->id);
        if ($movie) {
            $movie->update($request->except('_token'));
            return redirect()->route('movie.index')->with('success', __('Cập nhật phim thành công'));
        }
        return back()->with('error', __('Đã có lỗi xảy ra, vui lòng thử lại sau'));
    }

    public function delete(Request $request)
    {
        $movie = Movie::find($request->id);
        if ($movie) {
            $movie->delete();
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function requestUpdate(Request $request)
    {
        if (!$request->movie_id) return response()->json(['status' => false]);

        UserRequestUpdateMovie::create([
            'user_id' => auth()->user()->id,
            'movie_id' => $request->movie_id,
        ]);

        return response()->json(['status' => true]);
    }

    public function changeStatus(Request $request)
    {
        $movie = Movie::find($request->movie_id);
        if ($movie) {
            $movie->status = $movie->status === Movie::STATUS_ACTIVE ? Movie::STATUS_INACTIVE : Movie::STATUS_ACTIVE;
            $movie->save();
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }
}
