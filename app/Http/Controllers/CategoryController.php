<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\CreateCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::query()
            ->when($request->keyword, function ($q) use ($request) {
                $q->where('name', 'like', "%$request->keyword%")
                    ->orWhere('description', 'like', "%$request->keyword%");
            })
            ->latest('id')
            ->paginate(10);
        return view('category.index', compact('categories'));
    }

    public function store(CreateCategoryRequest $request)
    {
        $category = Category::create($request->except('_token'));
        if ($category) {
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('category.modal_update', compact('category'));
    }

    public function update(UpdateCategoryRequest $request)
    {
        $category = Category::find($request->id);
        if ($category) {
            $category->update($request->except('_token'));
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function delete(Request $request)
    {
        $category = Category::find($request->id);
        if ($category) {
            $category->delete();
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }
}
