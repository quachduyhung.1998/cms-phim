<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\Auth\ChangeProfileRequest;
use App\Http\Requests\Auth\ChangeUsernameRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Http\Requests\Auth\UpdatePasswordRequest;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function formLogin()
    {
        if (Auth::check()) {
            return redirect()->route('dashboard');
        } else {
            return view('auth.login');
        }
    }

    public function login(LoginRequest $request)
    {
        $credentials = [
            'username' => $request->username,
            'password' => $request->password
        ];

        if (Auth::attempt($credentials)) {
            if (auth()->user()->status === User::STATUS_INACTIVE) {
                auth()->logout();
                return back()->withErrors([
                    'username' => 'Tài khoản của bạn đã bị thu hồi.',
                ])->onlyInput('username');
            }
            $request->session()->regenerate();
            return redirect()->route('dashboard');
        }

        return back()->withErrors([
            'username' => 'Tài khoản hoặc mật khẩu không chính xác.',
        ])->onlyInput('username');
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/login');
    }

    public function requestResetPassword(ResetPasswordRequest $request)
    {
        $email = $request->input('email');

        $status = Password::sendResetLink(
            $request->only('email')
        );

        $user = User::query()->where('email', $email)->first();

        if ($user && $user->status === User::STATUS_ACTIVE) {
            $check = $status === Password::RESET_LINK_SENT
                ? back()->with(['status' => __($status)])
                : back()->withErrors(['email' => __($status)]);
            if (!$check) {
                return response()->json(['status' => false, 'message' => __('Đã có lỗi xảy ra, vui lòng thử lại sau')]);
            }
            return response()->json(['status' => true]);
        }

        return response()->json(['status' => false, 'message' => __('Email không tồn tại hoặc đã bị thu hồi')]);
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function (User $user, string $password) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));

                $user->save();

                event(new PasswordReset($user));
            }
        );

        return $status === Password::PASSWORD_RESET
            ? redirect()->route('login')->with('status_reset_password', __($status))
            : back()->withErrors(['email' => [__($status)]]);
    }

    public function profile()
    {
        $user = \auth()->user();
        return view('auth.profile', compact('user'));
    }

    public function changeProfile(ChangeProfileRequest $request)
    {
        $user = auth()->user();
        $user->full_name = $request->full_name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->save();

        return response()->json(['status' => true]);
    }

    public function changeUsername(ChangeUsernameRequest $request)
    {
        $user = auth()->user();
        if (!Hash::check($request->confirm_username_password, $user->password)) {
            throw \Illuminate\Validation\ValidationException::withMessages(['confirm_username_password' => 'Mật khẩu không chính xác']);
        }

        $user->username = $request->username;
        $user->save();

        return response()->json(['status' => true]);
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = auth()->user();
        if (!Hash::check($request->current_password, $user->password)) {
            throw \Illuminate\Validation\ValidationException::withMessages(['current_password' => 'Mật khẩu cũ không chính xác']);
        }

        $user->update(['password' => Hash::make($request->new_password)]);

        return response()->json(['status' => true]);
    }
}
