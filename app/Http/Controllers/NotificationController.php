<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\NotificationUser;
use App\Models\UserFirebaseToken;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function view()
    {
        NotificationUser::query()->where('user_id', auth()->user()->id)->update(['is_new' => NotificationUser::IS_OLD]);
    }

    public function click($id)
    {
        $notification = Notification::find($id);
        if ($notification) {
            $notificationUser = NotificationUser::query()
                ->where('notification_id', $notification->id)
                ->where('user_id', auth()->user()->id)
                ->first();
            if ($notificationUser && $notificationUser->status === NotificationUser::STATUS_UNREAD) {
                $notificationUser->is_new = NotificationUser::IS_OLD;
                $notificationUser->status = NotificationUser::STATUS_READ;
                $notificationUser->save();
            }
            $routeName = '';
            if ($notification->model_type === Notification::MODEL_TYPE_MOVIE) {
                $routeName = 'movie.view';
            } elseif ($notification->model_type === Notification::MODEL_TYPE_USER_REQUEST_UPDATE_MOVIE) {
                $routeName = 'user_request_update_movies.view';
            }
            return redirect()->route($routeName, ['id' => $notification->model_id]);
        }
        abort(404);
    }

    public function loadMore(Request $request)
    {
        $limit = 10;
        $offset = ($request->page - 1) * $limit;

        $notificationIds = NotificationUser::query()
            ->where('user_id', auth()->user()->id)
            ->pluck('notification_id')
            ->toArray();
        $notifications = Notification::with([
            'user',
            'movie',
            'userRequestUpdateMovie',
            'notificationUsers' => function ($q) {
                $q->where('user_id', auth()->user()->id);
            }
        ])
            ->where('id', '<=', $request->first_notification_id)
            ->whereIn('id', $notificationIds)
            ->latest('id')
            ->limit($limit)
            ->offset($offset)
            ->get();

        return view('layouts.notification_load_more', compact('notifications'));
    }

    public function saveToken(Request $request)
    {
        $checkExists = UserFirebaseToken::query()
            ->where('token', $request->fcm_token)
            ->first();
        if ($checkExists) {
            if ($checkExists->user_id !== auth()->user()->id) {
                $checkExists->user_id = auth()->user()->id;
                $checkExists->save();
            }
        } else {
            UserFirebaseToken::create([
                'user_id' => auth()->user()->id,
                'token' => $request->fcm_token,
            ]);
        }
    }
}
