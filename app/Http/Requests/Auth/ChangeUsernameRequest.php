<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class ChangeUsernameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'username' => [
                'required',
                Rule::unique('users')->ignore(auth()->user()->id)
            ],
            'confirm_username_password' => 'required',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'username' => Str::slug($this->username, ''),
        ]);
    }

    public function messages()
    {
        return [
            'username.required' => 'Bạn chưa nhập tên đăng nhập',
            'username.unique' => 'Tên đăng nhập đã tồn tại',
            'confirm_username_password.required' => 'Bạn chưa nhập mật khẩu xác nhận',
        ];
    }
}
