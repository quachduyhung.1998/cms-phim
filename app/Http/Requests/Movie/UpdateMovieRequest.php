<?php

namespace App\Http\Requests\Movie;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMovieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name_vn' => 'required|string',
            'name_en' => 'nullable|string',
        ];
    }

    public function messages()
    {
        return [
            'name_vn.required' => 'Bạn chưa tên phim tiếng Việt',
            'name_en.required' => 'Bạn chưa tên phim tiếng Anh',
        ];
    }
}
