<?php

namespace App\Http\Requests\User;

use app\Helpers\Helper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'full_name' => 'required|string',
            'username' => [
                'required',
                Rule::unique('users')->ignore($this->id)
            ],
            'role' => 'required',
            'email' => [
                'nullable',
                'email',
                Rule::unique('users')->ignore($this->id)
            ],
            'phone' => [
                'nullable',
                'regex:/^(03|05|07|08|09)[0-9]{8}+$/'
            ],
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'username' => Str::slug($this->username, ''),
        ]);
    }

    public function messages()
    {
        return [
            'full_name.required' => 'Bạn chưa nhập họ tên',
            'username.required' => 'Bạn chưa nhập tên đăng nhập',
            'username.unique' => 'Tên đăng nhập đã được sử dụng',
            'role.required' => 'Bạn chưa chọn vai trò',
            'email.email' => 'Email không đúng định dạng',
            'email.unique' => 'Email đã được sử dụng',
            'phone.regex' => 'Số điện thoại không đúng định dạng',
        ];
    }
}
