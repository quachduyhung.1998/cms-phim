<?php

namespace App\Observers;

use App\Models\Notification;
use App\Models\User;
use App\Models\UserRequestUpdateMovie;
use App\Services\Notification\NotificationService;

class UserRequestUpdateMovieObserver
{
    public function __construct(protected NotificationService $notificationService) {}

    /**
     * Handle the UserRequestUpdateMovie "created" event.
     */
    public function created(UserRequestUpdateMovie $userRequestUpdateMovie): void
    {
        $adminIds = User::query()->where('role', '!=', User::ROLE_BTV)->pluck('id')->toArray();
        $this->notificationService->create(auth()->user()->id, $adminIds, Notification::MODEL_TYPE_USER_REQUEST_UPDATE_MOVIE, $userRequestUpdateMovie->id);
    }

    /**
     * Handle the UserRequestUpdateMovie "updated" event.
     */
    public function updated(UserRequestUpdateMovie $userRequestUpdateMovie): void
    {
        $this->notificationService->create(auth()->user()->id, [$userRequestUpdateMovie->user_id], Notification::MODEL_TYPE_MOVIE, $userRequestUpdateMovie->movie_id, statusContent: $userRequestUpdateMovie->status);
    }

    /**
     * Handle the UserRequestUpdateMovie "deleted" event.
     */
    public function deleted(UserRequestUpdateMovie $userRequestUpdateMovie): void
    {
        //
    }

    /**
     * Handle the UserRequestUpdateMovie "restored" event.
     */
    public function restored(UserRequestUpdateMovie $userRequestUpdateMovie): void
    {
        //
    }

    /**
     * Handle the UserRequestUpdateMovie "force deleted" event.
     */
    public function forceDeleted(UserRequestUpdateMovie $userRequestUpdateMovie): void
    {
        //
    }
}
