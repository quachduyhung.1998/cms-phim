importScripts('https://www.gstatic.com/firebasejs/8.10.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.10.1/firebase-messaging.js');

firebase.initializeApp({
    apiKey: "AIzaSyDaEYugNZKmDh7Qp3bT6CAvuy_4dhKSzQk",
    projectId: "cms-phim-7d1ba",
    messagingSenderId: "485016653434",
    appId: "1:485016653434:web:d97dc3c73d1d1409ccb5c3"
});

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function({data}) {
    const notificationOptions= {
        body: data.body,
        icon: data.icon,
        image: data.image
    };
    return self.registration.showNotification(data.title, notificationOptions);
});
