<?php
return [
    'password_default' => '123456',
    'color' => [
        1 => 'primary',
        2 => 'info',
        3 => 'success',
        4 => 'warning',
        5 => 'danger',
        6 => 'dark',
    ],
];
