<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware(['guest'])->group(function () {
    Route::get('forgot-password', function () {
        return view('auth.forgot_password');
    })->name('password.request');
    Route::post('forgot-password', [\App\Http\Controllers\AuthController::class, 'requestResetPassword'])->name('password.email');
    Route::get('reset-password/{token}', function (string $token) {
        return view('auth.reset_password', ['token' => $token]);
    })->name('password.reset');
    Route::post('reset-password', [\App\Http\Controllers\AuthController::class, 'updatePassword'])->name('password.update');
});

Route::group(['prefix' => 'login'], function () {
    Route::get('', [\App\Http\Controllers\AuthController::class, 'formLogin'])->name('login');
    Route::post('', [\App\Http\Controllers\AuthController::class,'login']);
});

Route::get('logout', [\App\Http\Controllers\AuthController::class, 'logout'])->name('logout');

Route::middleware(['auth'])->group(function () {
    Route::get('', [\App\Http\Controllers\DashboardController::class, 'index'])
        ->name('dashboard')
        ->middleware('permission:view_dashboard');

    Route::post('upload-file', [\App\Http\Controllers\FileController::class, 'upload'])->name('file.upload');

    Route::group(['prefix' => 'notifications'], function () {
        Route::post('store', [\App\Http\Controllers\NotificationController::class, 'store'])->name('notification.store');
        Route::get('view', [\App\Http\Controllers\NotificationController::class, 'view'])->name('notification.view');
        Route::get('click/{id}', [\App\Http\Controllers\NotificationController::class, 'click'])->name('notification.click');
        Route::get('load-more', [\App\Http\Controllers\NotificationController::class, 'loadMore'])->name('notification.load_more');
        Route::post('/save-token', [\App\Http\Controllers\NotificationController::class, 'saveToken'])->name('notification.save_token');
    });

    Route::group(['prefix' => 'profile'], function () {
        Route::get('', [\App\Http\Controllers\AuthController::class, 'profile'])->name('profile');
        Route::put('change-profile', [\App\Http\Controllers\AuthController::class, 'changeProfile'])->name('profile.change_profile');
        Route::put('change-username', [\App\Http\Controllers\AuthController::class, 'changeUsername'])->name('profile.change_username');
        Route::put('change-password', [\App\Http\Controllers\AuthController::class, 'changePassword'])->name('profile.change_password');
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('', [\App\Http\Controllers\UserController::class, 'index'])
            ->name('user.index')
            ->middleware('permission:user.view');
        Route::post('store', [\App\Http\Controllers\UserController::class, 'store'])
            ->name('user.store')
            ->middleware('permission:user.create');
        Route::get('edit/{id}', [\App\Http\Controllers\UserController::class, 'edit'])
            ->name('user.edit')
            ->middleware('permission:user.edit');
        Route::post('update', [\App\Http\Controllers\UserController::class, 'update'])
            ->name('user.update')
            ->middleware('permission:user.edit');
        Route::put('change-status', [\App\Http\Controllers\UserController::class, 'changeStatus'])
            ->name('user.change_status')
            ->middleware('permission:user.edit');
        Route::put('reset-password', [\App\Http\Controllers\UserController::class, 'resetPassword'])
            ->name('user.reset_password')
            ->middleware('permission:user.edit');
        Route::delete('delete', [\App\Http\Controllers\UserController::class, 'delete'])
            ->name('user.delete')
            ->middleware('permission:user.delete');
    });

    Route::group(['prefix' => 'categories'], function () {
        Route::get('', [\App\Http\Controllers\CategoryController::class, 'index'])
            ->name('category.index')
            ->middleware('permission:category.view');
        Route::post('store', [\App\Http\Controllers\CategoryController::class, 'store'])
            ->name('category.store')
            ->middleware('permission:category.create');
        Route::get('edit/{id}', [\App\Http\Controllers\CategoryController::class, 'edit'])
            ->name('category.edit')
            ->middleware('permission:category.edit');
        Route::post('update', [\App\Http\Controllers\CategoryController::class, 'update'])
            ->name('category.update')
            ->middleware('permission:category.edit');
        Route::delete('delete', [\App\Http\Controllers\CategoryController::class, 'delete'])
            ->name('category.delete')
            ->middleware('permission:category.delete');
    });

    Route::group(['prefix' => 'providers'], function () {
        Route::get('', [\App\Http\Controllers\ProviderController::class, 'index'])
            ->name('provider.index')
            ->middleware('permission:provider.view');
        Route::post('store', [\App\Http\Controllers\ProviderController::class, 'store'])
            ->name('provider.store')
            ->middleware('permission:provider.create');
        Route::get('edit/{id}', [\App\Http\Controllers\ProviderController::class, 'edit'])
            ->name('provider.edit')
            ->middleware('permission:provider.edit');
        Route::post('update', [\App\Http\Controllers\ProviderController::class, 'update'])
            ->name('provider.update')
            ->middleware('permission:provider.edit');
        Route::delete('delete', [\App\Http\Controllers\ProviderController::class, 'delete'])
            ->name('provider.delete')
            ->middleware('permission:provider.delete');
    });

    Route::group(['prefix' => 'packages'], function () {
        Route::get('', [\App\Http\Controllers\PackageController::class, 'index'])
            ->name('package.index')
            ->middleware('permission:package.view');
        Route::post('store', [\App\Http\Controllers\PackageController::class, 'store'])
            ->name('package.store')
            ->middleware('permission:package.create');
        Route::get('edit/{id}', [\App\Http\Controllers\PackageController::class, 'edit'])
            ->name('package.edit')
            ->middleware('permission:package.edit');
        Route::post('update', [\App\Http\Controllers\PackageController::class, 'update'])
            ->name('package.update')
            ->middleware('permission:package.edit');
        Route::delete('delete', [\App\Http\Controllers\PackageController::class, 'delete'])
            ->name('package.delete')
            ->middleware('permission:package.delete');
        Route::post('get-package-by-provider-id', [\App\Http\Controllers\PackageController::class, 'getPackageByProviderId'])
            ->name('package.getPackageByProviderId')
            ->middleware('permission:package.view');
    });

    Route::group(['prefix' => 'actors'], function () {
        Route::get('', [\App\Http\Controllers\ActorController::class, 'index'])
            ->name('actor.index')
            ->middleware('permission:actor.view');
        Route::post('store', [\App\Http\Controllers\ActorController::class, 'store'])
            ->name('actor.store')
            ->middleware('permission:actor.create');
        Route::get('edit/{id}', [\App\Http\Controllers\ActorController::class, 'edit'])
            ->name('actor.edit')
            ->middleware('permission:actor.edit');
        Route::post('update', [\App\Http\Controllers\ActorController::class, 'update'])
            ->name('actor.update')
            ->middleware('permission:actor.edit');
        Route::delete('delete', [\App\Http\Controllers\ActorController::class, 'delete'])
            ->name('actor.delete')
            ->middleware('permission:actor.delete');
    });

    Route::group(['prefix' => 'movies'], function () {
        Route::get('', [\App\Http\Controllers\MovieController::class, 'index'])
            ->name('movie.index')
            ->middleware('permission:movie.view');
        Route::get('create', [\App\Http\Controllers\MovieController::class, 'create'])
            ->name('movie.create')
            ->middleware('permission:movie.create');
        Route::post('store', [\App\Http\Controllers\MovieController::class, 'store'])
            ->name('movie.store')
            ->middleware('permission:movie.create');
        Route::get('edit/{id}', [\App\Http\Controllers\MovieController::class, 'edit'])
            ->name('movie.edit')
            ->middleware('permission:movie.edit');
        Route::get('{id}', [\App\Http\Controllers\MovieController::class, 'view'])
            ->name('movie.view')
            ->middleware('permission:movie.view');
        Route::post('update/{id}', [\App\Http\Controllers\MovieController::class, 'update'])
            ->name('movie.update')
            ->middleware('permission:movie.edit');
        Route::delete('delete', [\App\Http\Controllers\MovieController::class, 'delete'])
            ->name('movie.delete')
            ->middleware('permission:movie.delete');
        Route::post('request-update', [\App\Http\Controllers\MovieController::class, 'requestUpdate'])
            ->name('movie.request_update')
            ->middleware('permission:movie.edit');
        Route::put('change-status', [\App\Http\Controllers\MovieController::class, 'changeStatus'])
            ->name('movie.change_status')
            ->middleware('permission:movie.edit');
    });

    Route::group(['prefix' => 'user-request-update-movie'], function () {
        Route::get('', [\App\Http\Controllers\UserRequestUpdateMovieController::class, 'index'])
            ->name('user_request_update_movies.index')
            ->middleware('permission:user_request_update_movie.view');
        Route::get('{id}', [\App\Http\Controllers\UserRequestUpdateMovieController::class, 'view'])
            ->name('user_request_update_movies.view')
            ->middleware('permission:user_request_update_movie.view');
        Route::post('update', [\App\Http\Controllers\UserRequestUpdateMovieController::class, 'update'])
            ->name('user_request_update_movies.update')
            ->middleware('permission:user_request_update_movie.edit');
    });
});
