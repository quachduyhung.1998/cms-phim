<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('movies', function (Blueprint $table) {
            $table->string('intro_start')->nullable()->change();
            $table->string('intro_end')->nullable()->change();
            $table->string('duration')->nullable()->change();
            $table->string('outro')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('movies', function (Blueprint $table) {
            $table->integer('intro_start')->nullable()->change();
            $table->integer('intro_end')->nullable()->change();
            $table->integer('duration')->nullable()->change();
            $table->integer('outro')->nullable()->change();
        });
    }
};
