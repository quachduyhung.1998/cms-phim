<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->id();
            $table->string('name_vn');
            $table->string('name_en')->nullable();
            $table->longText('images')->nullable();
            $table->string('practice')->nullable();
            $table->string('trailer')->nullable();
            $table->integer('intro_start')->nullable();
            $table->integer('intro_end')->nullable();
            $table->integer('outro')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->string('country')->nullable();
            $table->longText('vn_summary')->nullable();
            $table->integer('duration')->nullable();
            $table->year('release_date')->nullable();
            $table->string('rating')->nullable();
            $table->string('rating_vn')->nullable();
            $table->text('alert')->nullable();
            $table->text('genre')->nullable();
            $table->text('director_label')->nullable();
            $table->text('director_name')->nullable();
            $table->string('actor_ids')->nullable();
            $table->dateTime('available_from')->nullable();
            $table->dateTime('available_to')->nullable();
            $table->unsignedBigInteger('provider_id')->nullable();
            $table->unsignedBigInteger('package_id')->nullable();
            $table->integer('status')->default(1)->comment('1: mở, 2: đóng');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('movies');
    }
};
