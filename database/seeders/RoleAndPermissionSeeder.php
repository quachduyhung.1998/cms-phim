<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {// Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $arrayOfPermissionNames = [
            'view_dashboard',

            'permission.*',
            'permission.view',
            'permission.create',
            'permission.edit',
            'permission.delete',

            'user.*',
            'user.view',
            'user.create',
            'user.edit',
            'user.delete',

            'category.*',
            'category.view',
            'category.create',
            'category.edit',
            'category.delete',

            'provider.*',
            'provider.view',
            'provider.create',
            'provider.edit',
            'provider.delete',

            'package.*',
            'package.view',
            'package.create',
            'package.edit',
            'package.delete',

            'actor.*',
            'actor.view',
            'actor.create',
            'actor.edit',
            'actor.delete',

            'movie.*',
            'movie.view',
            'movie.create',
            'movie.edit',
            'movie.delete',

            'user_request_update_movie.*',
            'user_request_update_movie.view',
            'user_request_update_movie.create',
            'user_request_update_movie.edit',
            'user_request_update_movie.delete',
        ];
        $permissions = collect($arrayOfPermissionNames)->map(function ($permission) {
            return [
                'name' => $permission,
                'guard_name' => 'web',
                'created_at' => now(),
                'updated_at' => now()
            ];
        });
        Permission::insert($permissions->toArray());

        Role::create([
            'name' => 'super_admin',
            'guard_name' => 'web',
        ]);

        Role::create([
            'name' => 'admin',
            'guard_name' => 'web',
        ])->givePermissionTo([
            'view_dashboard',
            'category.*',
            'provider.*',
            'package.*',
            'actor.*',
            'movie.*',
            'user_request_update_movie.*'
        ]);

        Role::create([
            'name' => 'bien_tap_vien',
            'guard_name' => 'web',
        ])->givePermissionTo([
            'view_dashboard',
            'category.*',
            'provider.*',
            'package.*',
            'actor.*',
            'movie.*',
            'user_request_update_movie.create',
        ]);

        $superAdmin = User::query()->where('role', 1)->first();
        if ($superAdmin) $superAdmin->assignRole('super_admin');

        $admin = User::query()->where('role', 2)->first();
        if ($admin) $admin->assignRole('admin');

        $btv = User::query()->where('role', 3)->first();
        if ($btv) $btv->assignRole('bien_tap_vien');
    }
}
