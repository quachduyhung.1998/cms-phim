<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\User::create([
            'username' => 'superadmin',
            'full_name' => 'Super Admin',
            'status' => 1,
            'password' => Hash::make('123456'),
            'role' => 1
        ]);

        \App\Models\User::create([
            'username' => 'admin',
            'full_name' => 'Admin',
            'status' => 1,
            'password' => Hash::make('123456'),
            'role' => 2
        ]);

        \App\Models\User::create([
            'username' => 'bientapvien',
            'full_name' => 'Biên Tập Viên',
            'status' => 1,
            'password' => Hash::make('123456'),
            'role' => 3
        ]);
    }
}
